﻿drop table Ustanove

create table dbo.Ustanove (
sifraUstanove int not null identity(1,1) primary key, 
Naziv varchar(50) not null, 
Adresa varchar(50) not null, 
Active bit not null)

insert into Ustanove(Naziv,Adresa,Active) values ('Fakultet tehnickih nauka', 'Trg Dositeja Obradovića 6', 1)
insert into Ustanove(Naziv,Adresa,Active) values ('Ekonomski fakultet', 'Dr Sime Miloševića 16', 1)
insert into Ustanove(Naziv,Adresa,Active) values ('Medicinski fakultet', 'Hajduk Veljkova 3', 1)

select *
from Ustanove