﻿using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for AddEditTerminWindow.xaml
    /// </summary>
    public partial class AddEditTerminWindow : Window
    {
        public AddEditTerminWindow(Termin termin,Ustanova ustanova)
        {
            InitializeComponent();
            cmbTipNastave.ItemsSource = new List<ETipNastave>() { ETipNastave.PREDAVANJA, ETipNastave.VEZBE };
            foreach (Ucionica u in ustanova.Ucionice)
            {
                cmbUcionice.Items.Add(u.BrojUcionice);
            }
            cmbDan.ItemsSource = new List<EDani>() { EDani.PONEDELJAK, EDani.UTORAK, EDani.SREDA, EDani.CETVRTAK, EDani.PETAK, EDani.SUBOTA, EDani.NEDELJA, };
            Refresh();
            this.DataContext = termin;
        }

        private void cmbUcionice_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbUcionice.SelectedItem != null)
            {
                Ucionica ucionica=Data.DajUcionicuPoBroju(cmbUcionice.SelectedItem.ToString(),Data.ulogovaniKorisnik.Ustanova);
                Data.GenerisanjeNovihTermina(ucionica);
                Refresh();
            }
        }

        private void cmbPocetak_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbPocetak.SelectedItem != null)
            {
                DateTime p = (DateTime)cmbPocetak.SelectedItem;
                DateTime k = NadjiKrajTermina(p);
                cmbKraj.SelectedIndex = cmbKraj.Items.IndexOf(k);
                EDani dan = Data.pretvaranjeDana(p.DayOfWeek.ToString());
                cmbDan.SelectedIndex = cmbDan.Items.IndexOf(dan);
            }
        }

        private void Refresh()
        {
            cmbPocetak.Items.Clear();
            cmbKraj.Items.Clear();
            foreach (Termin t in Data.Termini)
            {
                if (cmbUcionice.SelectedItem != null && t.UcionicA!=null)
                {
                    if (t.UcionicA.BrojUcionice.Equals(cmbUcionice.SelectedItem.ToString()) && t.Zauzeo == null && DateTime.Now<t.PocetakTermina)
                    {
                        cmbPocetak.Items.Add(t.PocetakTermina);
                        cmbKraj.Items.Add(t.KrajTermina);
                    }
                }
            }
        }

        private DateTime NadjiKrajTermina(DateTime p)
        {
            foreach(Termin t in Data.Termini)
            {
                if (t.PocetakTermina == p && t.Zauzeo == null)
                {
                    return t.KrajTermina;
                }
            }
            return p;
        }

        private DateTime NadjiPocetakTermina(DateTime k)
        {
            foreach (Termin t in Data.Termini)
            {
                if (t.KrajTermina == k && t.Zauzeo==null)
                {
                    return t.PocetakTermina;
                }
            }
            return k;
        }

        private void cmbKraj_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbPocetak.SelectedItem != null)
            {
                DateTime k = (DateTime)cmbKraj.SelectedItem;
                DateTime p = NadjiPocetakTermina(k);
                cmbPocetak.SelectedIndex = cmbPocetak.Items.IndexOf(p);
                EDani dan = Data.pretvaranjeDana(k.DayOfWeek.ToString());
                cmbDan.SelectedIndex = cmbDan.Items.IndexOf(dan);
            }
        }

        private void cmbDan_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbDan.SelectedItem != null)
            {
                cmbPocetak.Items.Clear();
                cmbKraj.Items.Clear();
                EDani dan =(EDani) cmbDan.SelectedItem;
                foreach (Termin t in Data.Termini)
                {
                    if (cmbUcionice.SelectedItem != null && t.UcionicA != null)
                    {
                        if (t.Dan.ToString().Equals(dan.ToString()) && cmbUcionice.SelectedItem.ToString().Equals(t.UcionicA.BrojUcionice) && t.Zauzeo==null && DateTime.Now < t.PocetakTermina)
                        {
                            cmbPocetak.Items.Add(t.PocetakTermina);
                            cmbKraj.Items.Add(t.KrajTermina);
                        }
                    }
                }
            }
        }

        private void btnPotvrda_Click(object sender, RoutedEventArgs e)
        {
            if (ProveraPolja() == false)
            {
                return;
            }
            DateTime p = (DateTime)cmbPocetak.SelectedItem;
            DateTime k = (DateTime)cmbKraj.SelectedItem;

            foreach (Termin t in Data.Termini)
            {
                if (t.PocetakTermina==p && t.KrajTermina == k && cmbUcionice.SelectedItem.ToString().Equals(t.UcionicA.BrojUcionice))
                {
                    t.TipNastave = (ETipNastave)cmbTipNastave.SelectedItem;
                    t.Zauzeo = Data.ulogovaniKorisnik;
                    t.SacuvajTermin();
                    Refresh();
                    Data.ulogovaniKorisnik.Termini.Add(t);
                    MessageBox.Show($"Uspesno ste dodali termin!", "Potvrda", MessageBoxButton.OK);
                    return;
                }
            }
        }

        public bool ProveraPolja()
        {
            if(cmbUcionice.SelectedItem == null || cmbPocetak.SelectedItem == null || cmbKraj.SelectedItem == null || cmbDan.SelectedItem == null)
            {
                MessageBox.Show($"Sva polja moraju biti popunjena!", "Greska", MessageBoxButton.OK);
                return false;
            }
            return true;
        }
    }
}
