﻿using SF_27_2018_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace SF_27_2018_POP2019.Util
{
    public class Data
    {
        public static String Connection_string = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public static List<Korisnik> Korisnici { get; set; }
        public static List<Ustanova> Ustanove { get; set; }
        public static List<Profesor> Profesori { get; set; }
        public static List<Termin> Termini { get; set; }
        public static Korisnik ulogovaniKorisnik;
        public static List<Asistent> Asistenti { get; set; }

        public static Korisnik DajKorisnikaPoUserNameu(String username)
        {
            foreach (Korisnik korisnik in Korisnici)
            {
                if (korisnik.KorisnickoIme.Equals(username) && korisnik.Active==true)
                {
                    return korisnik;
                }
            }
            return null;
        }

        public static int DajIdKorisnika(String username)
        {
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select Id from Korisnici where KorisnickoIme like @username";
                command.Parameters.Add(new SqlParameter("username", "%" + username + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Korisnici");

                foreach (DataRow row in ds.Tables["Korisnici"].Rows)
                {
                    int idKorisnika = (int)row["Id"];
                    return idKorisnika;
                }

            }
            return 0;
        }

        public static Asistent DajAsistentaPoUserNameu(String username)
        {
            foreach (Asistent a in Asistenti)
            {
                if (a.KorisnickoIme.Equals(username))
                {
                    return a;
                }
            }
            return null;
        }

        public static Profesor DajProfesoraPoUserNameu(String username)
        {
            foreach (Profesor p in Profesori)
            {
                if (p.KorisnickoIme.Equals(username))
                {
                    return p;
                }
            }
            return null;
        }

        public static Ustanova DajUstanovuZaKorisnika(String userName)
        {
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select *
                                        from Ustanove
                                        where sifraUstanove=(select sifra_Ustanove					                                          
								                                from Korisnici
								                                where KorisnickoIme=@userName and Active=1)";
                command.Parameters.Add(new SqlParameter("userName", userName));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ustanove");
                foreach (DataRow row in ds.Tables["Ustanove"].Rows)
                {
                    int sifraUstanove = (int)row["sifraUstanove"];
                    string naziv = (string)row["Naziv"];
                    string adresa = (string)row["Adresa"];
                    bool active = (bool)row["Active"];

                    Ustanova ustanova = new Ustanova(sifraUstanove, naziv, adresa, active);
                    UcitavanjeUcionica(ustanova);
                    return ustanova;
                }
            }

            return null;
        }

        public static Ustanova DajUstanovuPoNazivu(String s)
        {
            foreach (Ustanova ustanova in Ustanove)
            {
                if (ustanova.NazivUstanove.Equals(s) && ustanova.Active==true)
                {
                    return ustanova;
                }
            }
            return null;
        }

        public static Ustanova DajUstanovuPoNazivuEdit(String s,Ustanova u)
        {
            foreach (Ustanova ustanova in Ustanove)
            {
                if (ustanova.NazivUstanove.Equals(s) && ustanova.Active == true && u.SifraUstanove!=ustanova.SifraUstanove)
                {
                    return ustanova;
                }
            }
            return null;
        }

        public static string NadjiProf(String userName)
        {
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select KorisnickoIme
                                        from Korisnici
                                        where Id=(select Profesor_Id
				                                    from Asistenti
				                                    where Id=(select Id
							                                    from Korisnici
							                                    where KorisnickoIme=@userName and Active=1))";
                command.Parameters.Add(new SqlParameter("userName", userName));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Korisnici");
                foreach (DataRow row in ds.Tables["Korisnici"].Rows)
                {
                    string korisnickoIme = (string)row["KorisnickoIme"];
                    return korisnickoIme;
                }
            }
            return null;
        }

        public static Korisnik NadjiKorisnika(int Id)
        {
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select *
                                        from Korisnici
                                        where Id=@Id";
                command.Parameters.Add(new SqlParameter("Id", Id));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Korisnici");
                foreach (DataRow row in ds.Tables["Korisnici"].Rows)
                {
                    string korisnickoIme = (string)row["KorisnickoIme"];
                    string ime = (string)row["Ime"];
                    string prezime = (string)row["Prezime"];
                    string lozinka = (string)row["Lozinka"];
                    bool active = (bool)row["Active"];
                    string email = (string)row["Email"];

                    if (row["TipKorisnika"].Equals(ETipKorisnika.ADMINISTRATOR.ToString()))
                    {
                        Administrator administrator = new Administrator(ime, prezime, email, korisnickoIme, lozinka, active);
                        return administrator;
                    }
                    if (row["TipKorisnika"].Equals(ETipKorisnika.PROFESOR.ToString()))
                    {
                        Profesor profesor = new Profesor(ime, prezime, email, korisnickoIme, lozinka, active);
                        profesor.Ustanova = DajUstanovuZaKorisnika(profesor.KorisnickoIme);
                        return profesor;
                    }
                    if (row["TipKorisnika"].Equals(ETipKorisnika.ASISTENT.ToString()))
                    {
                        string profesorKorisnicko = NadjiProf(korisnickoIme);
                        Profesor profesor = DajProfesoraPoUserNameu(profesorKorisnicko);
                        Asistent asistent = new Asistent(ime, prezime, email, korisnickoIme, lozinka, profesor, active);
                        asistent.Ustanova = DajUstanovuZaKorisnika(asistent.KorisnickoIme);
                        return asistent;
                    }
                }
            }
            return null;
        }

        public static Ucionica NadjiUcionicu(int sifraUcionice)
        {
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select sifraUcionice, brojUcionice, brojMesta, tipUcionice, uc.Active, sifraUstanove, Naziv, Adresa, us.Active
                                        from Ucionice uc, Ustanove us
                                        where sifraUcionice=@sifraUcionice and sifra_Ustanove=sifraUstanove";
                command.Parameters.Add(new SqlParameter("sifraUcionice", sifraUcionice));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ucionice");
                foreach (DataRow row in ds.Tables["Ucionice"].Rows)
                {
                    int sifraUcionice1 = (int)row["sifraUcionice"];
                    string brojUcionice = (string)row["brojUcionice"];
                    int brojMesta = (int)row["brojMesta"];
                    string tip = (string)row["tipUcionice"];
                    ETipUcionice tipUcionice = ETipUcionice.BezRacunara;
                    if (tip.Equals(ETipUcionice.SaRacunarima.ToString()))
                    {
                        tipUcionice = ETipUcionice.SaRacunarima;
                    }
                    bool active = (bool)row["Active"];
                    int sifraUstanove = (int)row["sifraUstanove"];
                    string naziv = (string)row["Naziv"];
                    string adresa = (string)row["Adresa"];
                    bool activeus = (bool)row["Active"];

                    Ustanova ustanova = new Ustanova(sifraUstanove, naziv, adresa, activeus);
                    Ucionica ucionica = new Ucionica(sifraUcionice1, brojUcionice, brojMesta, tipUcionice, ustanova, active);
                    return ucionica;
                }
            }
            return null;
        }


        public static Ucionica DajUcionicuPoBroju(String brojUcionice, Ustanova ustanova)
        {
            foreach (Ucionica ucionica in ustanova.Ucionice)
            {
                if (ucionica.BrojUcionice.Equals(brojUcionice) && ucionica.Active==true)
                {
                    return ucionica;
                }
            }
            return null;
        }

        public static Ucionica DajUcionicuPoBrojuZaEdit(String brojUcionice, Ustanova ustanova,Ucionica u)
        {
            foreach (Ucionica ucionica in ustanova.Ucionice)
            {
                if (ucionica.BrojUcionice.Equals(brojUcionice) && ucionica.Active == true && ucionica.SifraUcionice!=u.SifraUcionice)
                {
                    return ucionica;
                }
            }
            return null;
        }

        public static void UcitavanjeKorisnika()
        {
            Korisnici = new List<Korisnik>();

            using (SqlConnection conn=new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Korisnici where Active=1";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Korisnici");
                foreach(DataRow row in ds.Tables["Korisnici"].Rows)
                {
                    string korisnickoIme = (string) row["KorisnickoIme"];
                    string ime = (string)row["Ime"];
                    string prezime = (string)row["Prezime"];
                    string lozinka = (string)row["Lozinka"];
                    bool active = (bool)row["Active"];
                    string email = (string)row["Email"];

                    if (row["TipKorisnika"].Equals(ETipKorisnika.ADMINISTRATOR.ToString()))
                    {
                        Administrator administrator = new Administrator(ime,prezime,email, korisnickoIme, lozinka, active);
                        Korisnici.Add(administrator);
                    }
                    if (row["TipKorisnika"].Equals(ETipKorisnika.PROFESOR.ToString()))
                    {
                        Profesor profesor = new Profesor(ime, prezime, email, korisnickoIme, lozinka, active);
                        profesor.Ustanova = DajUstanovuZaKorisnika(profesor.KorisnickoIme);
                        Korisnici.Add(profesor);
                    }
                    if (row["TipKorisnika"].Equals(ETipKorisnika.ASISTENT.ToString()))
                    {
                        string profesorKorisnicko = NadjiProf(korisnickoIme);
                        Profesor profesor = new Profesor();
                        Asistent asistent = new Asistent(ime, prezime, email, korisnickoIme, lozinka,profesor, active);
                        asistent.Ustanova = DajUstanovuZaKorisnika(asistent.KorisnickoIme);
                        Korisnici.Add(asistent);
                    }
                }
            }
        }

        public static void UcitavanjeUstanova()
        {
            Ustanove = new List<Ustanova>();

            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Ustanove where Active=1";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ustanove");
                foreach (DataRow row in ds.Tables["Ustanove"].Rows)
                {
                    int sifraUstanove = (int)row["sifraUstanove"];
                    string naziv = (string)row["Naziv"];
                    string adresa = (string)row["Adresa"];
                    bool active = (bool)row["Active"];

                    Ustanova ustanova = new Ustanova(sifraUstanove, naziv, adresa, active);
                    UcitavanjeUcionica(ustanova);
                    Ustanove.Add(ustanova);
                }
            }
        }

        public static void UcitavanjeUcionica(Ustanova ustanova)
        {
            ustanova.Ucionice.Clear();
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select *
                                        from Ucionice
                                        where sifra_Ustanove=@sifraUstanove";
                command.Parameters.Add(new SqlParameter("sifraUstanove", ustanova.SifraUstanove));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ucionice");
                foreach (DataRow row in ds.Tables["Ucionice"].Rows)
                {
                    int sifraUcionice = (int)row["sifraUcionice"];
                    string brojUcionice = (string)row["brojUcionice"];
                    int brojMesta = (int)row["brojMesta"];
                    string tip = (string)row["tipUcionice"];
                    ETipUcionice tipUcionice = ETipUcionice.BezRacunara;
                    if (tip.Equals(ETipUcionice.SaRacunarima.ToString()))
                    {
                        tipUcionice = ETipUcionice.SaRacunarima;
                    }
                    bool active = (bool)row["Active"];

                    Ucionica ucionica = new Ucionica(sifraUcionice,brojUcionice,brojMesta,tipUcionice,ustanova,active);

                    ustanova.Ucionice.Add(ucionica);
                }
            }
        }

        public static void UcitavanjeTerminaZaKorisnika(Korisnik korisnik)
        {
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select *
                                        from Termini
                                        where sifra_Korisnika=(select Id from Korisnici where KorisnickoIme=@korisnicko) and Active=1";
                command.Parameters.Add(new SqlParameter("korisnicko", korisnik.KorisnickoIme));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termini");
                foreach (DataRow row in ds.Tables["Termini"].Rows)
                {
                    int sifraTermina = (int)row["sifraTermina"];
                    DateTime pocetakTermina = (DateTime)row["pocetakTermina"];
                    DateTime krajTermina = (DateTime)row["krajTermina"];
                    string d = (string)row["dan"];
                    EDani dan = EDani.PONEDELJAK;
                    switch (d)
                    {
                        case "UTORAK":
                            {
                                dan = EDani.UTORAK;
                            }
                            break;
                        case "SREDA":
                            {
                                dan = EDani.SREDA;
                            }
                            break;
                        case "CETVRTAK":
                            {
                                dan = EDani.CETVRTAK;
                            }
                            break;
                        case "PETAK":
                            {
                                dan = EDani.PETAK;
                            }
                            break;
                        case "SUBOTA":
                            {
                                dan = EDani.SUBOTA;
                            }
                            break;
                        case "NEDELJA":
                            {
                                dan = EDani.NEDELJA;
                            }
                            break;
                        default:
                            break;
                    }
                    string tip = (string)row["tipNastave"];
                    ETipNastave tipNastave = ETipNastave.PREDAVANJA;
                    if (tip.Equals("VEZBE"))
                    {
                        tipNastave = ETipNastave.VEZBE;
                    }
                    bool active = (bool)row["Active"];
                    int sifraKorisnika = (int)row["sifra_Korisnika"];
                    Korisnik korisnik1 = NadjiKorisnika(sifraKorisnika);
                    int sifraUcionice = (int)row["sifra_Ucionice"];
                    Ucionica ucionica = NadjiUcionicu(sifraUcionice);

                    Termin termin = new Termin(sifraTermina, pocetakTermina, krajTermina, dan, tipNastave, korisnik1, ucionica, active);

                    korisnik.Termini.Add(termin);
                }
            }
        }

        public static void UcitavanjeTermina()
        {
            Termini = new List<Termin>();
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select *
                                        from Termini where Active=1 and sifra_Ucionice in (select sifraUcionice from Ucionice where Active=1)";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termini");
                foreach (DataRow row in ds.Tables["Termini"].Rows)
                {
                    int sifraTermina = (int)row["sifraTermina"];
                    DateTime pocetakTermina = (DateTime)row["pocetakTermina"];
                    DateTime krajTermina = (DateTime)row["krajTermina"];
                    string d = (string)row["dan"];
                    EDani dan = EDani.PONEDELJAK;
                    switch (d)
                    {
                        case "UTORAK":
                            {
                                dan = EDani.UTORAK;
                            }
                            break;
                        case "SREDA":
                            {
                                dan = EDani.SREDA;
                            }
                            break;
                        case "CETVRTAK":
                            {
                                dan = EDani.CETVRTAK;
                            }
                            break;
                        case "PETAK":
                            {
                                dan = EDani.PETAK;
                            }
                            break;
                        case "SUBOTA":
                            {
                                dan = EDani.SUBOTA;
                            }
                            break;
                        case "NEDELJA":
                            {
                                dan = EDani.NEDELJA;
                            }
                            break;
                        default:
                            break;
                    }
                    string tip = (string)row["tipNastave"];
                    ETipNastave tipNastave = ETipNastave.PREDAVANJA;
                    if (tip.Equals("VEZBE"))
                    {
                        tipNastave = ETipNastave.VEZBE;
                    }
                    bool active = (bool)row["Active"];
                    int sifraKorisnika = (int)row["sifra_Korisnika"];
                    Korisnik korisnik1 = NadjiKorisnika(sifraKorisnika);
                    int sifraUcionice = (int)row["sifra_Ucionice"];
                    Ucionica ucionica = NadjiUcionicu(sifraUcionice);

                    Termin termin = new Termin(sifraTermina, pocetakTermina, krajTermina, dan, tipNastave, korisnik1, ucionica, active);
                    if (korisnik1 != null)
                    {
                        korisnik1.Termini.Add(termin);
                    }
                    Termini.Add(termin);
                }
            }
        }

        public static void PretragaKorisnika(string s,string parm, List<Korisnik> korisnici)
        {
            korisnici.Clear();
            using (SqlConnection conn=new SqlConnection(Connection_string)) 
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                if (parm.Equals("Ime"))
                {
                    command.CommandText = @"select * from Korisnici where Ime like @s";
                }
                if (parm.Equals("Prezime"))
                {
                    command.CommandText = @"select * from Korisnici where Prezime like @s";
                }
                if (parm.Equals("Email"))
                {
                    command.CommandText = @"select * from Korisnici where Email like @s";
                }
                if (parm.Equals("KorisnickoIme"))
                {
                    command.CommandText = @"select * from Korisnici where KorisnickoIme like @s";
                }
                if (parm.Equals("TipKorisnika"))
                {
                    command.CommandText = @"select * from Korisnici where TipKorisnika like @s";
                }
                command.Parameters.Add(new SqlParameter("s", "%" + s + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Korisnici");

                foreach (DataRow row in ds.Tables["Korisnici"].Rows)
                {
                    string korisnickoIme = (string)row["KorisnickoIme"];
                    string ime = (string)row["Ime"];
                    string prezime = (string)row["Prezime"];
                    string lozinka = (string)row["Lozinka"];
                    bool active = (bool)row["Active"];
                    string email = (string)row["Email"];

                    if (row["TipKorisnika"].Equals(ETipKorisnika.ADMINISTRATOR.ToString()))
                    {
                        Administrator administrator = new Administrator(ime, prezime, email, korisnickoIme, lozinka, active);
                        korisnici.Add(administrator);
                    }
                    if (row["TipKorisnika"].Equals(ETipKorisnika.PROFESOR.ToString()))
                    {
                        Profesor profesor = new Profesor(ime, prezime, email, korisnickoIme, lozinka, active);
                        korisnici.Add(profesor);
                    }
                    if (row["TipKorisnika"].Equals(ETipKorisnika.ASISTENT.ToString()))
                    {
                        string profesorKorisnicko = NadjiProf(korisnickoIme);
                        Profesor profesor = (Profesor)DajKorisnikaPoUserNameu(profesorKorisnicko);
                        Asistent asistent = new Asistent(ime, prezime, email, korisnickoIme, lozinka, profesor, active);
                        korisnici.Add(asistent);
                    }
                }

            }
        }

        public static void PretragaKorisnikaZaPosebnuUstanovu(string s, string parm, List<Korisnik> korisnici,Ustanova ustanova)
        {
            korisnici.Clear();
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                if (parm.Equals("Ime"))
                {
                    command.CommandText = @"select * from Korisnici where Ime like @s and sifra_Ustanove=@sifra_Ustanove and Active=1";
                }
                if (parm.Equals("Prezime"))
                {
                    command.CommandText = @"select * from Korisnici where Prezime like @s and sifra_Ustanove=@sifra_Ustanove and Active=1";
                }
                if (parm.Equals("Email"))
                {
                    command.CommandText = @"select * from Korisnici where Email like @s and sifra_Ustanove=@sifra_Ustanove and Active=1";
                }
                if (parm.Equals("KorisnickoIme"))
                {
                    command.CommandText = @"select * from Korisnici where KorisnickoIme like @s and sifra_Ustanove=@sifra_Ustanove and Active=1";
                }
                if (parm.Equals("TipKorisnika"))
                {
                    command.CommandText = @"select * from Korisnici where TipKorisnika like @s and sifra_Ustanove=@sifra_Ustanove and Active=1";
                }
                command.Parameters.Add(new SqlParameter("s", "%" + s + "%"));
                command.Parameters.Add(new SqlParameter("sifra_Ustanove", ustanova.SifraUstanove));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Korisnici");

                foreach (DataRow row in ds.Tables["Korisnici"].Rows)
                {
                    string korisnickoIme = (string)row["KorisnickoIme"];
                    string ime = (string)row["Ime"];
                    string prezime = (string)row["Prezime"];
                    string lozinka = (string)row["Lozinka"];
                    bool active = (bool)row["Active"];
                    string email = (string)row["Email"];

                    if (row["TipKorisnika"].Equals(ETipKorisnika.ADMINISTRATOR.ToString()))
                    {
                        Administrator administrator = new Administrator(ime, prezime, email, korisnickoIme, lozinka, active);
                        korisnici.Add(administrator);
                    }
                    if (row["TipKorisnika"].Equals(ETipKorisnika.PROFESOR.ToString()))
                    {
                        Profesor profesor = new Profesor(ime, prezime, email, korisnickoIme, lozinka, active);
                        korisnici.Add(profesor);
                    }
                    if (row["TipKorisnika"].Equals(ETipKorisnika.ASISTENT.ToString()))
                    {
                        string profesorKorisnicko = NadjiProf(korisnickoIme);
                        Profesor profesor = (Profesor)DajKorisnikaPoUserNameu(profesorKorisnicko);
                        Asistent asistent = new Asistent(ime, prezime, email, korisnickoIme, lozinka, profesor, active);
                        korisnici.Add(asistent);
                    }
                }

            }
        }

        public static void PretragaUstanova(string s, string parm, List<Ustanova> ustanove)
        {
            ustanove.Clear();
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                if (parm.Equals("Naziv"))
                {
                    command.CommandText = @"select * from Ustanove where Naziv like @s and Active=1";
                }
                if (parm.Equals("Adresa"))
                {
                    command.CommandText = @"select * from Ustanove where Adresa like @s and Active=1";
                }
                command.Parameters.Add(new SqlParameter("s", "%" + s + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ustanove");

                foreach (DataRow row in ds.Tables["Ustanove"].Rows)
                {
                    int sifraUstanove = (int)row["sifraUstanove"];
                    string nazivUstanove = (string)row["Naziv"];
                    string adresa = (string)row["Adresa"];
                    bool active = (bool)row["Active"];

                    Ustanova ustanova = new Ustanova(sifraUstanove, nazivUstanove, adresa, active);
                    ustanove.Add(ustanova);
                }

            }
        }

        public static void PretragaUcionica(string s,Ustanova ustanova,string parm)
        {
            ustanova.Ucionice.Clear();
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                if (parm.Equals("brojUcionice"))
                {
                    command.CommandText = @"select * from Ucionice where brojUcionice LIKE @s and sifra_Ustanove=@sifraUstanove and Active=1";
                }

                if (parm.Equals("brojMesta"))
                {
                    command.CommandText = @"select * from Ucionice where brojMesta LIKE @s and sifra_Ustanove=@sifraUstanove and Active=1";
                }
                if (parm.Equals("tipUcionice"))
                {
                    command.CommandText = @"select * from Ucionice where tipUcionice LIKE @s and sifra_Ustanove=@sifraUstanove and Active=1";
                }
                command.Parameters.Add(new SqlParameter("s", "%"+ s + "%"));
                command.Parameters.Add(new SqlParameter("sifraUstanove", ustanova.SifraUstanove));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ucionice");

                foreach (DataRow row in ds.Tables["Ucionice"].Rows)
                {
                    int sifraUcionice = (int)row["sifraUcionice"];
                    string brojUcionice = (string)row["brojUcionice"];
                    int brojMesta = (int)row["brojMesta"];
                    string tip = (string)row["tipUcionice"];
                    ETipUcionice tipUcionice = ETipUcionice.BezRacunara;
                    if (tip.Equals(ETipUcionice.SaRacunarima.ToString()))
                    {
                        tipUcionice = ETipUcionice.SaRacunarima;
                    }
                    bool active = (bool)row["Active"];

                    Ucionica ucionica = new Ucionica(sifraUcionice, brojUcionice, brojMesta, tipUcionice, ustanova, active);
                    ustanova.Ucionice.Add(ucionica);
                }

            }
        }

        public static void PretragaTermina(string s, string parm, List<Termin> termini)
        {
            termini.Clear();
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                if (parm.Equals("sifra_Ucionice"))
                {
                    command.CommandText = @"select *
                                        from Termini
                                        where sifra_Ucionice in (select sifraUcionice from Ucionice where brojUcionice like @s)  and Active=1";
                }
                if (parm.Equals("sifra_Korisnika"))
                {
                    command.CommandText = @"select *
                                        from Termini
                                        where sifra_Korisnika in (select Id from Korisnici where KorisnickoIme like @s)  and Active=1";
                }
                if (parm.Equals("dan"))
                {
                    command.CommandText = @"select *
                                        from Termini
                                        where dan like @s  and Active=1";
                }
                if (parm.Equals("tipNastave"))
                {
                    command.CommandText = @"select *
                                        from Termini
                                        where tipNastave like @s  and Active=1";
                }
                if (parm.Equals("ustanova"))
                {
                    command.CommandText = @"select *
                                        from Termini
                                        where sifra_Ucionice in (select sifraUcionice from Ucionice where sifra_Ustanove like @s) and Active=1";
                }
                command.Parameters.Add(new SqlParameter("s", "%" + s + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termini");
                foreach (DataRow row in ds.Tables["Termini"].Rows)
                {
                    int sifraTermina = (int)row["sifraTermina"];
                    DateTime pocetakTermina = (DateTime)row["pocetakTermina"];
                    DateTime krajTermina = (DateTime)row["krajTermina"];
                    string d = (string)row["dan"];
                    EDani dan = EDani.PONEDELJAK;
                    switch (d)
                    {
                        case "UTORAK":
                            {
                                dan = EDani.UTORAK;
                            }
                            break;
                        case "SREDA":
                            {
                                dan = EDani.SREDA;
                            }
                            break;
                        case "CETVRTAK":
                            {
                                dan = EDani.CETVRTAK;
                            }
                            break;
                        case "PETAK":
                            {
                                dan = EDani.PETAK;
                            }
                            break;
                        case "SUBOTA":
                            {
                                dan = EDani.SUBOTA;
                            }
                            break;
                        case "NEDELJA":
                            {
                                dan = EDani.NEDELJA;
                            }
                            break;
                        default:
                            break;
                    }
                    string tip = (string)row["tipNastave"];
                    ETipNastave tipNastave = ETipNastave.PREDAVANJA;
                    if (tip.Equals("VEZBE"))
                    {
                        tipNastave = ETipNastave.VEZBE;
                    }
                    bool active = (bool)row["Active"];
                    int sifraKorisnika = (int)row["sifra_Korisnika"];
                    Korisnik korisnik1 = NadjiKorisnika(sifraKorisnika);
                    int sifraUcionice = (int)row["sifra_Ucionice"];
                    Ucionica ucionica = NadjiUcionicu(sifraUcionice);

                    Termin termin = new Termin(sifraTermina, pocetakTermina, krajTermina, dan, tipNastave, korisnik1, ucionica, active);
                    if (korisnik1 != null)
                    {
                        korisnik1.Termini.Add(termin);
                    }
                    Termini.Add(termin);
                }
            }
        }

        public static void PretragaTerminaPoDatumu(string s, string parm, List<Termin> termini)
        {
            termini.Clear();
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select *
                                        from Termini where Active=1";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termini");
                foreach (DataRow row in ds.Tables["Termini"].Rows)
                {
                    int sifraTermina = (int)row["sifraTermina"];
                    DateTime pocetakTermina = (DateTime)row["pocetakTermina"];
                    DateTime krajTermina = (DateTime)row["krajTermina"];
                    string d = (string)row["dan"];
                    EDani dan = EDani.PONEDELJAK;
                    switch (d)
                    {
                        case "UTORAK":
                            {
                                dan = EDani.UTORAK;
                            }
                            break;
                        case "SREDA":
                            {
                                dan = EDani.SREDA;
                            }
                            break;
                        case "CETVRTAK":
                            {
                                dan = EDani.CETVRTAK;
                            }
                            break;
                        case "PETAK":
                            {
                                dan = EDani.PETAK;
                            }
                            break;
                        case "SUBOTA":
                            {
                                dan = EDani.SUBOTA;
                            }
                            break;
                        case "NEDELJA":
                            {
                                dan = EDani.NEDELJA;
                            }
                            break;
                        default:
                            break;
                    }
                    string tip = (string)row["tipNastave"];
                    ETipNastave tipNastave = ETipNastave.PREDAVANJA;
                    if (tip.Equals("VEZBE"))
                    {
                        tipNastave = ETipNastave.VEZBE;
                    }
                    bool active = (bool)row["Active"];
                    int sifraKorisnika = (int)row["sifra_Korisnika"];
                    Korisnik korisnik1 = NadjiKorisnika(sifraKorisnika);
                    int sifraUcionice = (int)row["sifra_Ucionice"];
                    Ucionica ucionica = NadjiUcionicu(sifraUcionice);

                    Termin termin = new Termin(sifraTermina, pocetakTermina, krajTermina, dan, tipNastave, korisnik1, ucionica, active);
                    if (korisnik1 != null)
                    {
                        korisnik1.Termini.Add(termin);
                    }
                    if (parm.Equals("pocetakTermina") && termin.PocetakTermina.ToString().Contains(s))
                    {
                        Termini.Add(termin);
                    }
                    if (parm.Equals("krajTermina") && termin.KrajTermina.ToString().Contains(s))
                    {
                        Termini.Add(termin);
                    }
                }
            }
        }

        public static Korisnik Prijava(String korisnicko,String lozinka1)
        {
            foreach (Korisnik k in Korisnici)
            {
                if (k.KorisnickoIme.Equals(korisnicko) && k.Lozinka.Equals(lozinka1) && k.Active == true)
                    return k;
            }
            return null;
        }

        public static void GenerisanjeNovihTermina(Ucionica ucionica)
        {
            DateTime danasnjiDatum = DateTime.Now;
            DateTime maxVremeZaDanasnji = danasnjiDatum.AddDays(7);
            while (danasnjiDatum <= maxVremeZaDanasnji)
            {
                DateTime p = new DateTime(danasnjiDatum.Year, danasnjiDatum.Month, danasnjiDatum.Day, 7, 0, 0);
                DateTime k = p.AddMinutes(45);
                for (int i = 0; i < 20; i++)
                {
                    Termin termin = new Termin();
                    termin.PocetakTermina = p;
                    termin.KrajTermina = k;
                    termin.Dan = pretvaranjeDana(p.DayOfWeek.ToString());
                    termin.TipNastave = ETipNastave.PREDAVANJA;
                    termin.Zauzeo = null;
                    termin.UcionicA = ucionica;
                    termin.Active = true;
                    //Console.WriteLine("Provera da li je danasnji dan veci od kraja termina!");
                    //Console.WriteLine("Ucionica: " + ucionica.BrojUcionice);
                    //Console.WriteLine("Pocetak termina:");
                    //Console.WriteLine(p);
                    if (ProveriTermine(termin,ucionica) == false)
                    {
                        Termini.Add(termin);
                        p = p.AddMinutes(50);
                        k = p.AddMinutes(45);
                    }
                    else
                    {
                        p = p.AddMinutes(50);
                        k = p.AddMinutes(45);
                        //Console.WriteLine("Izlazim iz funkcije!");
                        continue;
                    }
                }
                danasnjiDatum = danasnjiDatum.AddDays(1);
            }
        }

        public static bool ProveriTermine(Termin termin, Ucionica ucionica)
        {
            foreach(Termin t in Termini)
            {
                //Console.WriteLine("Pocetak termina koji postoji: "+termin.PocetakTermina);
                if (t.PocetakTermina==termin.PocetakTermina && t.KrajTermina == termin.KrajTermina && ucionica.SifraUcionice==t.UcionicA.SifraUcionice && t.Active==true)
                {
                    //Console.WriteLine("Vracam true za pocetak termina: " + termin.PocetakTermina);
                    //Console.WriteLine("-----------------------\n");
                    return true;
                }
            }
            return false;
        }

        public static EDani pretvaranjeDana(String dan)
        {
            EDani danUNedelji=EDani.PONEDELJAK;
            switch (dan)
            {
                case "Monday":
                    {
                        danUNedelji = EDani.PONEDELJAK;
                    }
                    break;
                case "Tuesday":
                    {
                        danUNedelji = EDani.UTORAK;
                    }
                    break;
                case "Wednesday":
                    {
                        danUNedelji = EDani.SREDA;
                    }
                    break;
                case "Thursday":
                    {
                        danUNedelji = EDani.CETVRTAK;
                    }
                    break;
                case "Friday":
                    {
                        danUNedelji = EDani.PETAK;
                    }
                    break;
                case "Saturday":
                    {
                        danUNedelji = EDani.SUBOTA;
                    }
                    break;
                case "Sunday":
                    {
                        danUNedelji = EDani.NEDELJA;
                    }
                    break;
                default:
                    break;
            }
            return danUNedelji;
        }

        public static int DajSifruUcionice()
        {
            int maxSifra = 0;
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select max(sifraUcionice) as MAX
                                        from Ucionice";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ucionice");
                foreach (DataRow row in ds.Tables["Ucionice"].Rows)
                {
                    maxSifra = (int)row["MAX"];
                }
            }
            return maxSifra + 1;
        }

        public static DateTime PocetakToDateTime(String s)
        {
            string pocetak = s.Split('-').GetValue(0).ToString().Trim();
            string datum = pocetak.Split(' ').GetValue(0).ToString();
            int mesec = Int32.Parse(datum.Split('/').GetValue(0).ToString());
            int dan = Int32.Parse(datum.Split('/').GetValue(1).ToString());
            int godina = Int32.Parse(datum.Split('/').GetValue(2).ToString());
            DateTime p = new DateTime(godina,mesec,dan);
            return p;
        }

        public static DateTime KrajToDateTime(String s)
        {
            string pocetak = s.Split('-').GetValue(1).ToString().Trim();
            string datum = pocetak.Split(' ').GetValue(0).ToString();
            int mesec = Int32.Parse(datum.Split('/').GetValue(0).ToString());
            int dan = Int32.Parse(datum.Split('/').GetValue(1).ToString());
            int godina = Int32.Parse(datum.Split('/').GetValue(2).ToString());
            DateTime k = new DateTime(godina, mesec, dan, 23,59,59);
            return k;
        }

        public static List<Termin> DajTermineZaNedelju(DateTime p,DateTime k,Ustanova ustanova)
        {
            List<Termin> termini= new List<Termin>();
            using (SqlConnection conn = new SqlConnection(Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select *
                                        from Termini
                                        where sifra_Korisnika !=0 and sifra_Ucionice in (select sifraUcionice
						                                        from Ucionice
						                                        where sifra_Ustanove=@sifra_Ustanove) and 
                                                                pocetakTermina between @pocetakNedelje and @krajNedelje and Active=1";
                command.Parameters.Add(new SqlParameter("sifra_Ustanove", ustanova.SifraUstanove));
                command.Parameters.Add(new SqlParameter("pocetakNedelje", p));
                command.Parameters.Add(new SqlParameter("krajNedelje", k));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termini");
                foreach (DataRow row in ds.Tables["Termini"].Rows)
                {
                    int sifraTermina = (int)row["sifraTermina"];
                    DateTime pocetakTermina = (DateTime)row["pocetakTermina"];
                    DateTime krajTermina = (DateTime)row["krajTermina"];
                    string d = (string)row["dan"];
                    EDani dan = EDani.PONEDELJAK;
                    switch (d)
                    {
                        case "UTORAK":
                            {
                                dan = EDani.UTORAK;
                            }
                            break;
                        case "SREDA":
                            {
                                dan = EDani.SREDA;
                            }
                            break;
                        case "CETVRTAK":
                            {
                                dan = EDani.CETVRTAK;
                            }
                            break;
                        case "PETAK":
                            {
                                dan = EDani.PETAK;
                            }
                            break;
                        case "SUBOTA":
                            {
                                dan = EDani.SUBOTA;
                            }
                            break;
                        case "NEDELJA":
                            {
                                dan = EDani.NEDELJA;
                            }
                            break;
                        default:
                            break;
                    }
                    string tip = (string)row["tipNastave"];
                    ETipNastave tipNastave = ETipNastave.PREDAVANJA;
                    if (tip.Equals("VEZBE"))
                    {
                        tipNastave = ETipNastave.VEZBE;
                    }
                    bool active = (bool)row["Active"];
                    int sifraKorisnika = (int)row["sifra_Korisnika"];
                    Korisnik korisnik1 = NadjiKorisnika(sifraKorisnika);
                    int sifraUcionice = (int)row["sifra_Ucionice"];
                    Ucionica ucionica = NadjiUcionicu(sifraUcionice);

                    Termin termin = new Termin(sifraTermina, pocetakTermina, krajTermina, dan, tipNastave, korisnik1, ucionica, active);
                    if (korisnik1 != null)
                    {
                        korisnik1.Termini.Add(termin);
                    }
                    termini.Add(termin);
                }
            }
            return termini;
        }

        public static void UcitavanjeProfesora()
        {
            Console.WriteLine("Ucitavam profesore......");
            Profesori =new List<Profesor>();
            foreach(Korisnik k in Korisnici)
            {
                if (k.TipKorisnika.ToString().Equals("PROFESOR"))
                {
                    Profesor profesor = new Profesor(k.Ime, k.Prezime, k.Email, k.KorisnickoIme, k.Lozinka, k.Active);
                    profesor.Ustanova = k.Ustanova;
                    Profesori.Add(profesor);
                }
            }
        }

        public static void UcitavanjeAsistenata()
        {
            Console.WriteLine("Ucitavam asistente......");
            Asistenti = new List<Asistent>();
            foreach (Korisnik k in Korisnici)
            {
                if (k.TipKorisnika.ToString().Equals("ASISTENT"))
                {
                    string pkorisnicko = NadjiProf(k.KorisnickoIme);
                    Profesor profesor = DajProfesoraPoUserNameu(pkorisnicko);
                    Asistent asistent = new Asistent(k.Ime, k.Prezime, k.Email, k.KorisnickoIme, k.Lozinka,profesor, k.Active);
                    asistent.Ustanova = k.Ustanova;
                    if (profesor != null)
                    {
                        profesor.Asistenti.Add(asistent);
                    }
                    else
                    {
                        Asistenti.Add(asistent);
                    }
                }
            }
        }

        public static void GenerisiTermineZaUstanovu(Ucionica ucionica)
        {
            //Console.WriteLine(ucionica.BrojUcionice);
            //Console.WriteLine("---------------------\n");
            DateTime maxDanasnjiDan = DateTime.Now.AddDays(28);
            DateTime danasnjiDan = DateTime.Now;
            DateTime k;
            DateTime p;
            Termin termin;
            foreach(Termin t in Termini)
            {
                //Console.WriteLine("Sledeci termin!!\n+++++++++++++++++++++");
                if (t.KrajTermina < danasnjiDan && t.UcionicA.SifraUcionice == ucionica.SifraUcionice)
                {
                    p = t.PocetakTermina;
                    k = t.KrajTermina;
                    DateTime d = danasnjiDan;
                    while (d <= maxDanasnjiDan)
                    {
                        //Console.WriteLine("Pocetak termina: " + p);
                        //Console.WriteLine("Kraj termina: " + k);
                        //Console.WriteLine("***********************************\n");
                        p = p.AddDays(7);
                        k = k.AddDays(7);
                        termin = new Termin();
                        termin.PocetakTermina = p;
                        termin.KrajTermina = k;
                        termin.Dan = pretvaranjeDana(p.DayOfWeek.ToString());
                        termin.TipNastave = t.TipNastave;
                        termin.Active = true;
                        termin.UcionicA = t.UcionicA;
                        termin.Zauzeo = t.Zauzeo;
                        if (ProveriTermine(termin, ucionica) == false)
                        {
                            if (danasnjiDan <= termin.PocetakTermina)
                            {
                                Console.WriteLine("U ifu sam!");
                                //Console.WriteLine("Pocetak termina: " + p);
                                //Console.WriteLine("Kraj termina: " + k);
                                //Console.WriteLine("***********************************\n");
                                termin.SacuvajTermin();
                            }
                            UcitavanjeTermina();
                        }
                        d = d.AddDays(7);
                    }
                }
                else if ((t.KrajTermina - maxDanasnjiDan).TotalDays < 28 && t.UcionicA.SifraUcionice == ucionica.SifraUcionice)
                {
                    p = t.PocetakTermina;
                    k = t.KrajTermina;
                    while (p <= maxDanasnjiDan)
                    {
                        //Console.WriteLine("Pocetak termina: " + p);
                        //Console.WriteLine("Kraj termina: " + k);
                        //Console.WriteLine("***********************************\n");
                        p = p.AddDays(7);
                        k = k.AddDays(7);
                        termin = new Termin();
                        termin.PocetakTermina = p;
                        termin.KrajTermina = k;
                        termin.Dan = pretvaranjeDana(p.DayOfWeek.ToString());
                        termin.TipNastave = t.TipNastave;
                        termin.Active = true;
                        termin.UcionicA = t.UcionicA;
                        termin.Zauzeo = t.Zauzeo;
                        if (ProveriTermine(termin, ucionica) == false)
                        {
                            Console.WriteLine("U else sam!");
                            //Console.WriteLine("Pocetak termina: " + p);
                            //Console.WriteLine("Kraj termina: " + k);
                            //Console.WriteLine("***********************************\n");
                            termin.SacuvajTermin();
                            UcitavanjeTermina();
                        }
                    }
                }
            }
        }
    }
}
