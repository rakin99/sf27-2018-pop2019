﻿using SF_27_2018_POP2019.Util;
using System.Windows;


namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Data.UcitavanjeKorisnika();
            Data.UcitavanjeProfesora();
            Data.UcitavanjeAsistenata();
            Data.UcitavanjeUstanova();
            Data.UcitavanjeTermina();

            Prijava prijava = new Prijava();
            prijava.ShowDialog();
        }
    }
}
