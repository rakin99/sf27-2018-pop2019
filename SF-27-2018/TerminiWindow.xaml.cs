﻿using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for TerminiWindow.xaml
    /// </summary>
    public partial class TerminiWindow : Window
    {
        ICollectionView view;
        string selectedFilter = "Active";
        public TerminiWindow()
        {
            InitializeComponent();
            dgTermini.IsReadOnly = true;
            if (Data.ulogovaniKorisnik.TipKorisnika.ToString().Equals("ASISTENT"))
            {
                btnAsistenti.Visibility = Visibility.Hidden;
            }
            InitializeView();
            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {
            Termin termin = obj as Termin;
            return termin.Active;
        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.ulogovaniKorisnik.Termini);

            //binding kolekcija na data grid pri cemu se kolona ucionica rucno definisu
            DataGridTextColumn column1 = new DataGridTextColumn();
            column1.Header = "Ucionica";
            column1.Width = new DataGridLength(1, DataGridLengthUnitType.Star);
            column1.Binding = new Binding("UcionicA.BrojUcionice");
            dgTermini.Columns.Add(column1);

            dgTermini.ItemsSource = view;
        }

        private void dgTermini_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Datum") || e.PropertyName.Equals("UcionicA") || e.PropertyName.Equals("Zauzeo") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("SifraTermina"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void btnProfil_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = Data.ulogovaniKorisnik;

            UserAddEditWindow userWindow = new UserAddEditWindow(korisnik,"prikaz");
            userWindow.ShowDialog();
        }

        private void btnAsistenti_Click(object sender, RoutedEventArgs e)
        {
            AsistentiWindow asistentiWindow = new AsistentiWindow();
            asistentiWindow.ShowDialog();
        }

        private void btnDodajTermin_Click(object sender, RoutedEventArgs e)
        {
            Termin termin = new Termin();
            AddEditTerminWindow addEditTerminWindow = new AddEditTerminWindow(termin,Data.ulogovaniKorisnik.Ustanova);
            addEditTerminWindow.ShowDialog();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Termin termin = dgTermini.SelectedItem as Termin;
            if (termin == null)
            {
                MessageBox.Show("Termin nije selektovan", "Greska", MessageBoxButton.OK);
            }
            else
            {
                termin.DeleteTermin();
                Data.ulogovaniKorisnik.Termini.Remove(termin);
                Termin t = nadjiTermin(termin);
                Data.Termini.Remove(t);
                selectedFilter = "Active";
                view.Refresh();
            }
        }

        private void btnRaspored_Click(object sender, RoutedEventArgs e)
        {
            RasporediWindow rasporediWindow = new RasporediWindow();
            rasporediWindow.ShowDialog();
        }

        private Termin nadjiTermin(Termin termin)
        {
            foreach(Termin t in Data.Termini)
            {
                if (t.SifraTermina == termin.SifraTermina)
                {
                    return t;
                }
            }
            return null;
        }
    }
}
