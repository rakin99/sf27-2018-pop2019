﻿using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnUser_Click(object sender, RoutedEventArgs e)
        {
            UserWindow mainWindow = new UserWindow();
            mainWindow.ShowDialog();
        }

        private void btnUstanove_Click(object sender, RoutedEventArgs e)
        {
            Data.UcitavanjeTermina();
            UstanoveWindow ustanoveWindow = new UstanoveWindow();
            ustanoveWindow.ShowDialog();
        }

        private void btnRasporedi_Click(object sender, RoutedEventArgs e)
        {
            RasporediWindow rasporediWindow = new RasporediWindow();
            rasporediWindow.ShowDialog();
        }

        private void btnTermini_Click(object sender, RoutedEventArgs e)
        {
            SviTerminiWindow sviTerminiWindow = new SviTerminiWindow();
            sviTerminiWindow.ShowDialog();
        }
    }
}
