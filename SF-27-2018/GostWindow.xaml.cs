﻿using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for GostWindow.xaml
    /// </summary>
    public partial class GostWindow : Window
    {
        public GostWindow()
        {
            InitializeComponent();
            Administrator administrator = new Administrator();
            Data.ulogovaniKorisnik = administrator;
        }

        private void btnUstanove_Click(object sender, RoutedEventArgs e)
        {
            UstanoveWindow ustanove = new UstanoveWindow();
            ustanove.ShowDialog();
        }

        private void btnRaspored_Click(object sender, RoutedEventArgs e)
        {
            RasporediWindow rasporediWindow = new RasporediWindow();
            rasporediWindow.ShowDialog();
        }

        private void btnZaposleni_Click(object sender, RoutedEventArgs e)
        {
            ZaposleniWindow zaposleniWindow = new ZaposleniWindow();
            zaposleniWindow.ShowDialog();
        }
    }
}
