﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;
using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for UserWindow.xaml
    /// </summary>
    public partial class UserWindow : Window
    {
        ICollectionView view;
        string selectedFilter = "Active";
        public UserWindow()
        {
            InitializeComponent();
            dgUsers.IsReadOnly = true;
            RefreshParametara();
            InitializeView();
            view.Filter = CustomFilter;
        }

        private bool CustomFilter (object obj)
        {
            Korisnik user = obj as Korisnik;
            return user.Active;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Administrator administrator = new Administrator();
            UserAddEditWindow userWindow = new UserAddEditWindow(administrator,"");
            if (userWindow.ShowDialog() == true)
            {
                selectedFilter = "Active";
                view.Refresh();
            }
        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Korisnici);
            dgUsers.ItemsSource = view;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Korisnik user = dgUsers.SelectedValue as Korisnik;
            if(user != null)
            {
                user.DeleteKorisnika();

                selectedFilter = "Active";
                view.Refresh();
            }
            else
            {
                MessageBox.Show("Selektujte korisnika kojeg zelite obrisati!","Warning",MessageBoxButton.OK);
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Korisnik user = dgUsers.SelectedItem as Korisnik;

            if (user == null)
            {
                MessageBox.Show("Korisnik nije selektovan", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Korisnik oldUser = user.Clone();
                UserAddEditWindow userWindow = new UserAddEditWindow(user,"");
                if (userWindow.ShowDialog() == false)
                {
                    int indeks = Data.Korisnici.FindIndex(u => u.KorisnickoIme.Equals(oldUser.KorisnickoIme));
                    Data.Korisnici[indeks] = oldUser;
                }
                selectedFilter = "Active";
                view.Refresh();
            }
        }

        private void dgUsers_AutoGeneratingColumn(object sender, System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Ustanova") || e.PropertyName.Equals("Termini") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("Id"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        public void RefreshParametara()
        {
            cmbParametar.Items.Add("imenu");
            cmbParametar.Items.Add("prezimenu");
            cmbParametar.Items.Add("email-u");
            cmbParametar.Items.Add("korisnickom imenu");
            cmbParametar.Items.Add("tipu korisnika");
            cmbParametar.SelectedIndex = 0;
        }

        private string DajParametar()
        {
            String parametarPretrage="";
            if (cmbParametar.SelectedItem != null)
            {
                switch (cmbParametar.SelectedItem.ToString())
                {
                    case "imenu":
                        {
                            parametarPretrage = "Ime";
                        }
                        break;
                    case "prezimenu":
                        {
                            parametarPretrage = "Prezime";
                        }
                        break;
                    case "email-u":
                        {
                            parametarPretrage = "Email";
                        }
                        break;
                    case "korisnickom imenu":
                        {
                            parametarPretrage = "KorisnickoIme";
                        }
                        break;
                    case "tipu korisnika":
                        {
                            parametarPretrage = "TipKorisnika";
                        }
                        break;
                    default:
                        break;
                }
            }
            return parametarPretrage;
        }

        private void txtSearch_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Data.PretragaKorisnika(txtSearch.Text, DajParametar(),Data.Korisnici);
            view.Refresh();
        }
    }
}
