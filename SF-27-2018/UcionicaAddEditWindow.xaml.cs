﻿using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for UcionicaAddEditWindow.xaml
    /// </summary>
    public partial class UcionicaAddEditWindow : Window
    {
        enum Status { ADD, EDIT }
        private Status _status;
        private Ucionica selectedUcionica;
        
        public UcionicaAddEditWindow(Ucionica ucionica)
        {
            InitializeComponent();

            cmbTip.ItemsSource = new List<ETipUcionice>() { ETipUcionice.BezRacunara, ETipUcionice.SaRacunarima };

            if (ucionica.BrojUcionice.Equals(""))
            {
                this._status = Status.ADD;
            }
            else
            {
                this._status = Status.EDIT;
            }
            selectedUcionica = ucionica;
            this.DataContext = ucionica;
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            Ucionica ucionica = Data.DajUcionicuPoBroju(txtBrUcionice.Text, selectedUcionica.Ustanova);
            if (ucionica != null && _status.Equals(Status.ADD))
            {
                MessageBox.Show($"Ucionica sa brojem {ucionica.BrojUcionice} vec postoji", "Greska", MessageBoxButton.OK);
                return;
            }
            if (ProveraPolja() == false)
            {
                return;
            }
            if (_status.Equals(Status.ADD))
            {
                Console.WriteLine("Sifra je: "+Data.DajSifruUcionice());
                selectedUcionica.SacuvajUcionicu();
                Data.UcitavanjeUcionica(selectedUcionica.Ustanova);
            }
            if (_status.Equals(Status.EDIT))
            {
                ucionica = Data.DajUcionicuPoBrojuZaEdit(txtBrUcionice.Text, selectedUcionica.Ustanova,selectedUcionica);
                if (ucionica != null && _status.Equals(Status.EDIT))
                {
                    MessageBox.Show($"Ucionica sa brojem {ucionica.BrojUcionice} vec postoji", "Greska", MessageBoxButton.OK);
                    return;
                }
                selectedUcionica.UpdateUcionicu();
            }
            this.DialogResult = true;
            this.Close();
        }

        public bool ProveraPolja()
        {
            if (txtBrMesta.Text.Equals("0"))
            {
                MessageBox.Show($"Polje broj mesta ne moze biti nula!", "Greska", MessageBoxButton.OK);
                return false;
            }
            if (txtBrUcionice.Text.Equals("") && txtBrMesta.Text.Equals(""))
            {
                MessageBox.Show($"Sva polja moraju biti popunjena!", "Greska", MessageBoxButton.OK);
                return false;
            }
            return true;
        }
    }
}
