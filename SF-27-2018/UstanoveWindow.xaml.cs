﻿using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for UstanoveWindow.xaml
    /// </summary>
    public partial class UstanoveWindow : Window
    {
        ICollectionView view;
        string selectedFilter = "Active";
        public UstanoveWindow()
        {
            InitializeComponent();
            dgUstanove.IsReadOnly = true;
            InitializeView();
            RefreshParametara();

            if (Data.ulogovaniKorisnik.KorisnickoIme.Equals(""))
            {
                btnDelete.Visibility = Visibility.Hidden;
                btnEdit.Visibility = Visibility.Hidden;
                btnUcionice.Visibility = Visibility.Hidden;
                btdAdd.Visibility = Visibility.Hidden;
                btnGenerisanjeTermina.Visibility = Visibility.Hidden;
            }

            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {
            Ustanova ustanova = obj as Ustanova;
            return ustanova.Active;
        }


        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Ustanove);
            dgUstanove.ItemsSource = view;
        }

        private void btdAdd_Click(object sender, RoutedEventArgs e)
        {
            Ustanova ustanova = new Ustanova();
            UstanovaAddEditWindow ustanovaWindow = new UstanovaAddEditWindow(ustanova);
            if (ustanovaWindow.ShowDialog() == true)
            {
                selectedFilter = "Active";
                view.Refresh();
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Ustanova ustanova = dgUstanove.SelectedItem as Ustanova;
            if (ustanova == null)
            {
                MessageBox.Show("Ustanova nije selektovana", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Ustanova oldUstanova = ustanova.Clone();
                UstanovaAddEditWindow ustanovaWindow = new UstanovaAddEditWindow(ustanova);
                if (ustanovaWindow.ShowDialog() == false)
                {
                    int indeks = Data.Ustanove.FindIndex(u => u.SifraUstanove.Equals(oldUstanova.SifraUstanove));
                    Data.Ustanove[indeks] = oldUstanova;
                }
                selectedFilter = "Active";
                view.Refresh();
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Ustanova ustanova = dgUstanove.SelectedItem as Ustanova;
            if (ustanova != null)
            {
                foreach(Ucionica u in ustanova.Ucionice)
                {
                    u.DeleteUcionicu();
                }
                Data.Ustanove.Remove(ustanova);
                ustanova.DeleteUstanovu();

                selectedFilter = "Active";
                view.Refresh();
            }
            else
            {
                MessageBox.Show("Selektujte ustanovu koju zelite obrisati", "Warning", MessageBoxButton.OK);
            }
        }

        private void btnUcionice_Click(object sender, RoutedEventArgs e)
        {
            Ustanova ustanova = dgUstanove.SelectedItem as Ustanova;
            if (ustanova == null)
            {
                MessageBox.Show("Ustanova nije selektovana", "Warning", MessageBoxButton.OK);
            }
            else
            {
                UcioniceWindow ucioniceWindow = new UcioniceWindow(ustanova);
                if (ucioniceWindow.ShowDialog() == true)
                {
                    selectedFilter = "Active";
                    view.Refresh();
                }
            }
        }

        private void dgUstanove_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Ucionice") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("SifraUstanove"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        public void RefreshParametara()
        {
            cmbParametar.Items.Add("nazivu");
            cmbParametar.Items.Add("adresi");
            cmbParametar.SelectedIndex = 0;
        }

        private string DajParametar()
        {
            String parametarPretrage = "";
            if (cmbParametar.SelectedItem != null)
            {
                switch (cmbParametar.SelectedItem.ToString())
                {
                    case "nazivu":
                        {
                            parametarPretrage = "Naziv";
                        }
                        break;
                    case "adresi":
                        {
                            parametarPretrage = "Adresa";
                        }
                        break;
                    default:
                        break;
                }
            }
            return parametarPretrage;
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            Data.PretragaUstanova(txtSearch.Text, DajParametar(),Data.Ustanove);
            view.Refresh();
        }

        private void btnGenerisanjeTermina_Click(object sender, RoutedEventArgs e)
        {
            Ustanova ustanova = dgUstanove.SelectedItem as Ustanova;
            if (ustanova == null)
            {
                MessageBox.Show("Ni jedna ustanova nije selektovana!", "Greska", MessageBoxButton.OK);
            }
            else
            {
                foreach (Ucionica u in ustanova.Ucionice)
                {
                    Data.GenerisiTermineZaUstanovu(u);
                }
                MessageBox.Show("Uspesno ste generisali termine", "Potvrda", MessageBoxButton.OK);
            }
        }
    }
}
