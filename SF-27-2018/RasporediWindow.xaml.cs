﻿using SF_27_2018.models;
using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for RasporediWindow.xaml
    /// </summary>
    public partial class RasporediWindow : Window
    {
        ICollectionView view;
        public RasporediWindow()
        {
            InitializeComponent();
            dgRaspored.IsReadOnly = true;
            foreach (Ustanova u in Data.Ustanove)
            {
                cmbUstanova.Items.Add(u.NazivUstanove);
            }
            List<Sedmica> sedmice = GenerisanjeSedmica();
            foreach (Sedmica s in sedmice)
            {
                cmbSedmica.Items.Add(s.ToString());
            }
            InitializeView(DateTime.Now.ToString()+" - "+DateTime.Now.ToString());
        }

        private void InitializeView(String s)
        {
            if(Data.ulogovaniKorisnik.TipKorisnika.ToString().Equals("PROFESOR") || Data.ulogovaniKorisnik.TipKorisnika.ToString().Equals("ASISTENT")) 
            {
                cmbUstanova.SelectedIndex = cmbUstanova.Items.IndexOf(Data.ulogovaniKorisnik.Ustanova.NazivUstanove);
                cmbUstanova.IsEnabled = false;
            }
            dgRaspored.Columns.Clear();
            DateTime pocetak = Data.PocetakToDateTime(s);
            DateTime kraj = Data.KrajToDateTime(s);
            Ustanova ustanova = new Ustanova();
            if (cmbUstanova.SelectedItem != null)
            {
                ustanova = Data.DajUstanovuPoNazivu(cmbUstanova.SelectedItem.ToString());
            }
            List<Termin> termini = Data.DajTermineZaNedelju(pocetak,kraj,ustanova);
            view = CollectionViewSource.GetDefaultView(NapraviRaspored(s));

            DataGridTextColumn ponedeljak = new DataGridTextColumn();
            ponedeljak.Header = "Ponedeljak";
            dgRaspored.Columns.Add(ponedeljak);

            DataGridTextColumn utorak = new DataGridTextColumn();
            utorak.Header = "Utorak";
            dgRaspored.Columns.Add(utorak);

            DataGridTextColumn sreda = new DataGridTextColumn();
            sreda.Header = "Sreda";
            dgRaspored.Columns.Add(sreda);

            DataGridTextColumn cetvrtak = new DataGridTextColumn();
            cetvrtak.Header = "Cetvrtak";
            dgRaspored.Columns.Add(cetvrtak);

            DataGridTextColumn petak = new DataGridTextColumn();
            petak.Header = "Petak";
            dgRaspored.Columns.Add(petak);

            DataGridTextColumn subota = new DataGridTextColumn();
            subota.Header = "Subota";
            dgRaspored.Columns.Add(subota);

            DataGridTextColumn nedelja = new DataGridTextColumn();
            nedelja.Header = "Nedelja";
            dgRaspored.Columns.Add(nedelja);

            if (cmbSedmica.SelectedItem != null)
            {
                foreach (Termin t in termini)
                {
                    if (t.Dan.ToString().Equals("PONEDELJAK"))
                    {
                        ponedeljak.Binding = new Binding("Ponedeljak.Datum");
                    }
                    else if (t.Dan.ToString().Equals("UTORAK"))
                    {
                        utorak.Binding = new Binding("Utorak.Datum");
                    }
                    else if (t.Dan.ToString().Equals("SREDA"))
                    {
                        sreda.Binding = new Binding("Sreda.Datum");
                    }
                    else if (t.Dan.ToString().Equals("CETVRTAK"))
                    {
                        cetvrtak.Binding = new Binding("Cetvrtak.Datum");
                    }
                    else if (t.Dan.ToString().Equals("PETAK"))
                    {
                        petak.Binding = new Binding("Petak.Datum");
                    }
                    else if (t.Dan.ToString().Equals("SUBOTA"))
                    {
                        subota.Binding = new Binding("Subota.Datum");
                    }
                    else if(t.Dan.ToString().Equals("NEDELJA"))
                    {
                        nedelja.Binding = new Binding("Nedelja.Datum");
                    }
                }
            }

            dgRaspored.ItemsSource = view;
        }

        public static List<Sedmica> GenerisanjeSedmica()
        {
            List<Sedmica> sedmice = new List<Sedmica>();
            int godina = DateTime.Now.Year;
            DateTime prviDanNedelje = new DateTime(godina, 1, 1);
            DateTime ponedeljak= prviDanNedelje;
            DateTime nedelja = prviDanNedelje;
            while (!nedelja.DayOfWeek.ToString().Equals("Sunday"))
            {
                nedelja = nedelja.AddDays(1);
            }
            while (!ponedeljak.DayOfWeek.ToString().Equals("Monday"))
            {
                ponedeljak = ponedeljak.AddDays(1);
            }
            Sedmica sedmica = new Sedmica(prviDanNedelje, nedelja.AddHours(23).AddMinutes(59));
            sedmice.Add(sedmica);
            DateTime sledecaGod = new DateTime(godina+1, 1, 1);
            while (ponedeljak < sledecaGod)
            {
                nedelja = ponedeljak.AddDays(6).AddHours(23).AddMinutes(59);
                Sedmica s = new Sedmica(ponedeljak, nedelja);
                sedmice.Add(s);
                ponedeljak = ponedeljak.AddDays(7);
            }
            return sedmice;
        }

        private void cmbSedmica_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbSedmica.SelectedItem != null)
            {
                string selectovanaNed = cmbSedmica.SelectedItem.ToString();
                InitializeView(selectovanaNed);
            }
        }


        private void dgRaspored_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Ponedeljak") || e.PropertyName.Equals("Utorak") || e.PropertyName.Equals("Sreda") || e.PropertyName.Equals("Cetvrtak") || e.PropertyName.Equals("Petak") || e.PropertyName.Equals("Subota") || e.PropertyName.Equals("Nedelja"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        public void ObrisiSveKolone(DataGridColumn dan)
        {
            dgRaspored.Columns.Remove(dan);
        }

        private List<Raspored> NapraviRaspored(String s)
        {
            List<Raspored> rasporedi = new List<Raspored>();
            DateTime pocetak = Data.PocetakToDateTime(s);
            DateTime kraj = Data.KrajToDateTime(s);
            Ustanova ustanova = new Ustanova();
            if (cmbUstanova.SelectedItem != null)
            {
                ustanova = Data.DajUstanovuPoNazivu(cmbUstanova.SelectedItem.ToString());
            }
            List<Termin> termini = Data.DajTermineZaNedelju(pocetak,kraj, ustanova);
            Raspored raspored = new Raspored();
            foreach (Termin t in termini)
            {
                if (t.Dan.ToString().Equals("PONEDELJAK"))
                {
                    raspored = PogledajPonedeljak(rasporedi);
                    if (raspored != null)
                    {
                        if (raspored.Ponedeljak.Zauzeo.Equals(""))
                        {
                            raspored.Ponedeljak.Datum = t.ToString();
                            raspored.Ponedeljak.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Ponedeljak.Ucionica = t.UcionicA.BrojUcionice;
                        }
                        else
                        {
                            raspored = new Raspored();
                            raspored.Ponedeljak.Datum = t.ToString();
                            raspored.Ponedeljak.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Ponedeljak.Ucionica = t.UcionicA.BrojUcionice;
                            rasporedi.Add(raspored);
                        }
                    }
                    else
                    {
                        raspored = new Raspored();
                        raspored.Ponedeljak.Datum = t.ToString();
                        raspored.Ponedeljak.Zauzeo = t.Zauzeo.KorisnickoIme;
                        raspored.Ponedeljak.Ucionica = t.UcionicA.BrojUcionice;
                        rasporedi.Add(raspored);
                    }
                }
                if (t.Dan.ToString().Equals("UTORAK"))
                {
                    raspored = PogledajUtorak(rasporedi);
                    if (raspored != null)
                    {
                        if (raspored.Utorak.Zauzeo.Equals(""))
                        {
                            raspored.Utorak.Datum = t.ToString();
                            raspored.Utorak.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Utorak.Ucionica = t.UcionicA.BrojUcionice;
                        }
                        else
                        {
                            raspored = new Raspored();
                            raspored.Utorak.Datum = t.ToString();
                            raspored.Utorak.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Utorak.Ucionica = t.UcionicA.BrojUcionice;
                            rasporedi.Add(raspored);
                        }
                    }
                    else
                    {
                        raspored = new Raspored();
                        raspored.Utorak.Datum = t.ToString();
                        raspored.Utorak.Zauzeo = t.Zauzeo.KorisnickoIme;
                        raspored.Utorak.Ucionica = t.UcionicA.BrojUcionice;
                        rasporedi.Add(raspored);
                    }
                }
                if (t.Dan.ToString().Equals("SREDA"))
                {
                    raspored = PogledajSredu(rasporedi);
                    if (raspored != null)
                    {
                        if (raspored.Sreda.Zauzeo.Equals(""))
                        {
                            raspored.Sreda.Datum = t.ToString();
                            raspored.Sreda.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Sreda.Ucionica = t.UcionicA.BrojUcionice;
                        }
                        else
                        {
                            raspored = new Raspored();
                            raspored.Sreda.Datum = t.ToString();
                            raspored.Sreda.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Sreda.Ucionica = t.UcionicA.BrojUcionice;
                            rasporedi.Add(raspored);
                        }
                    }
                    else
                    {
                        raspored = new Raspored();
                        raspored.Sreda.Datum = t.ToString();
                        raspored.Sreda.Zauzeo = t.Zauzeo.KorisnickoIme;
                        raspored.Sreda.Ucionica = t.UcionicA.BrojUcionice;
                        rasporedi.Add(raspored);
                    }
                }
                if (t.Dan.ToString().Equals("CETVRTAK"))
                {
                    raspored = PogledajCetvrtak(rasporedi);
                    if (raspored != null)
                    {
                        if (raspored.Cetvrtak.Zauzeo.Equals(""))
                        {
                            raspored.Cetvrtak.Datum = t.ToString();
                            raspored.Cetvrtak.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Cetvrtak.Ucionica = t.UcionicA.BrojUcionice;
                        }
                        else
                        {
                            Console.WriteLine("U elseu sam kad je raspored prazan string");
                            raspored = new Raspored();
                            raspored.Cetvrtak.Datum = t.ToString();
                            raspored.Cetvrtak.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Cetvrtak.Ucionica = t.UcionicA.BrojUcionice;
                            rasporedi.Add(raspored);
                        }
                    }
                    else
                    {
                        raspored = new Raspored();
                        raspored.Cetvrtak.Datum = t.ToString();
                        raspored.Cetvrtak.Zauzeo = t.Zauzeo.KorisnickoIme;
                        raspored.Cetvrtak.Ucionica = t.UcionicA.BrojUcionice;
                        rasporedi.Add(raspored);
                    }
                }
                if (t.Dan.ToString().Equals("PETAK"))
                {
                    raspored = PogledajPetak(rasporedi);
                    if (raspored != null)
                    {
                        if (raspored.Petak.Zauzeo.Equals(""))
                        {
                            raspored.Petak.Datum = t.ToString();
                            raspored.Petak.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Petak.Ucionica = t.UcionicA.BrojUcionice;
                        }
                        else
                        {
                            raspored = new Raspored();
                            raspored.Petak.Datum = t.ToString();
                            raspored.Petak.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Petak.Ucionica = t.UcionicA.BrojUcionice;
                            rasporedi.Add(raspored);
                        }
                    }
                    else
                    {
                        raspored = new Raspored();
                        raspored.Petak.Datum = t.ToString();
                        raspored.Petak.Zauzeo = t.Zauzeo.KorisnickoIme;
                        raspored.Petak.Ucionica = t.UcionicA.BrojUcionice;
                        rasporedi.Add(raspored);
                    }
                }
                if (t.Dan.ToString().Equals("SUBOTA"))
                {
                    raspored = PogledajSubotu(rasporedi);
                    if (raspored != null)
                    {
                        if (raspored.Subota.Zauzeo.Equals(""))
                        {
                            raspored.Subota.Datum = t.ToString();
                            raspored.Subota.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Subota.Ucionica = t.UcionicA.BrojUcionice;
                        }
                        else
                        {
                            raspored = new Raspored();
                            raspored.Subota.Datum = t.ToString();
                            raspored.Subota.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Subota.Ucionica = t.UcionicA.BrojUcionice;
                            rasporedi.Add(raspored);
                        }
                    }
                    else
                    {
                        raspored = new Raspored();
                        raspored.Subota.Datum = t.ToString();
                        raspored.Subota.Zauzeo = t.Zauzeo.KorisnickoIme;
                        raspored.Subota.Ucionica = t.UcionicA.BrojUcionice;
                        rasporedi.Add(raspored);
                    }
                }
                if (t.Dan.ToString().Equals("NEDELJA"))
                {
                    raspored = PogledajNedelju(rasporedi);
                    if (raspored != null)
                    {
                        if (raspored.Nedelja.Zauzeo.Equals(""))
                        {
                            raspored.Nedelja.Datum = t.ToString();
                            raspored.Nedelja.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Nedelja.Ucionica = t.UcionicA.BrojUcionice;
                        }
                        else
                        {
                            raspored = new Raspored();
                            raspored.Nedelja.Datum = t.ToString();
                            raspored.Nedelja.Zauzeo = t.Zauzeo.KorisnickoIme;
                            raspored.Nedelja.Ucionica = t.UcionicA.BrojUcionice;
                            rasporedi.Add(raspored);
                        }
                    }
                    else
                    {
                        raspored = new Raspored();
                        raspored.Nedelja.Datum = t.ToString();
                        raspored.Nedelja.Zauzeo = t.Zauzeo.KorisnickoIme;
                        raspored.Nedelja.Ucionica = t.UcionicA.BrojUcionice;
                        rasporedi.Add(raspored);
                    }
                }
            }
            return rasporedi;
        }

        public Raspored PogledajPonedeljak(List<Raspored> rasporedi)
        {
            foreach (Raspored r in rasporedi)
            {
                if (r.Ponedeljak.Zauzeo.Equals(""))
                {
                    return r;
                }
            }
            return null;
        }

        public Raspored PogledajUtorak(List<Raspored> rasporedi)
        {
            foreach (Raspored r in rasporedi)
            {
                if (r.Utorak.Zauzeo.Equals(""))
                {
                    return r;
                }
            }
            return null;
        }

        public Raspored PogledajSredu(List<Raspored> rasporedi)
        {
            foreach (Raspored r in rasporedi)
            {
                if (r.Sreda.Zauzeo.Equals(""))
                {
                    return r;
                }
            }
            return null;
        }

        public Raspored PogledajCetvrtak(List<Raspored> rasporedi)
        {
            foreach (Raspored r in rasporedi)
            {
                if (r.Cetvrtak.Zauzeo.Equals(""))
                {
                    return r;
                }
            }
            return null;
        }

        public Raspored PogledajPetak(List<Raspored> rasporedi)
        {
            foreach (Raspored r in rasporedi)
            {
                if (r.Petak.Zauzeo.Equals(""))
                {
                    return r;
                }
            }
            return null;
        }

        public Raspored PogledajSubotu(List<Raspored> rasporedi)
        {
            foreach (Raspored r in rasporedi)
            {
                if (r.Subota.Zauzeo.Equals(""))
                {
                    return r;
                }
            }
            return null;
        }

        public Raspored PogledajNedelju(List<Raspored> rasporedi)
        {
            foreach (Raspored r in rasporedi)
            {
                if (r.Nedelja.Zauzeo.Equals(""))
                {
                    return r;
                }
            }
            return null;
        }

        private void cmbUstanova_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbSedmica.SelectedItem != null)
            {
                string selectovanaNed = cmbSedmica.SelectedItem.ToString();
                InitializeView(selectovanaNed);
            }
        }
    }
}
