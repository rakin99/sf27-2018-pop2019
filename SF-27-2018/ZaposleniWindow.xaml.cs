﻿using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for ZaposleniWindow.xaml
    /// </summary>
    public partial class ZaposleniWindow : Window
    {
        List<Korisnik> zaposleni = new List<Korisnik>();
        ICollectionView view;
        string selectedFilter = "Active";
        public ZaposleniWindow()
        {
            InitializeComponent();
            dgZaposleni.IsReadOnly = true;
            RefreshParametara();
            
            RefreshUstanove();
        }

        private bool CustomFilter(object obj)
        {
            Korisnik user = obj as Korisnik;
            return user.Active;
        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(zaposleni);
            dgZaposleni.ItemsSource = view;
        }

        public List<Korisnik> DajZaposlene()
        {
            Console.WriteLine("Generisanje zaposlenih");
            List<Korisnik> zaposleni = new List<Korisnik>();
            if (cmbUstanove.SelectedItem != null)
            {
                Ustanova ustanova = Data.DajUstanovuPoNazivu(cmbUstanove.SelectedItem.ToString());
                foreach (Profesor p in Data.Profesori)
                {
                    if (p.Ustanova.SifraUstanove==ustanova.SifraUstanove)
                    {
                        zaposleni.Add(p);
                        foreach (Asistent a in p.Asistenti)
                        {
                            if (a.Ustanova.SifraUstanove == ustanova.SifraUstanove)
                            {
                                zaposleni.Add(a);
                            }
                        }
                    }
                }
                foreach (Asistent a in Data.Asistenti)
                {
                    if (a.Ustanova.SifraUstanove == ustanova.SifraUstanove)
                    {
                        zaposleni.Add(a);
                    }
                }
            }
            return zaposleni;
        }

        public void RefreshUstanove()
        {
            foreach (Ustanova u in Data.Ustanove)
            {
                cmbUstanove.Items.Add(u.NazivUstanove);
            }
        }

        private void dgZaposleni_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Lozinka") || e.PropertyName.Equals("Ustanova") || e.PropertyName.Equals("Termini") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("Id"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void cmbUstanove_DropDownClosed(object sender, EventArgs e)
        {
            zaposleni = DajZaposlene();
            InitializeView();
            view.Filter = CustomFilter;
        }

        public void RefreshParametara()
        {
            cmbParametar.Items.Add("imenu");
            cmbParametar.Items.Add("prezimenu");
            cmbParametar.Items.Add("email-u");
            cmbParametar.Items.Add("korisnickom imenu");
            cmbParametar.Items.Add("tipu korisnika");
            cmbParametar.SelectedIndex = 0;
        }

        private string DajParametar()
        {
            String parametarPretrage = "";
            if (cmbParametar.SelectedItem != null)
            {
                switch (cmbParametar.SelectedItem.ToString())
                {
                    case "imenu":
                        {
                            parametarPretrage = "Ime";
                        }
                        break;
                    case "prezimenu":
                        {
                            parametarPretrage = "Prezime";
                        }
                        break;
                    case "email-u":
                        {
                            parametarPretrage = "Email";
                        }
                        break;
                    case "korisnickom imenu":
                        {
                            parametarPretrage = "KorisnickoIme";
                        }
                        break;
                    case "tipu korisnika":
                        {
                            parametarPretrage = "TipKorisnika";
                        }
                        break;
                    default:
                        break;
                }
            }
            return parametarPretrage;
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (cmbUstanove.SelectedItem != null)
            {
                Ustanova ustanova = Data.DajUstanovuPoNazivu(cmbUstanove.SelectedItem.ToString());
                Data.PretragaKorisnikaZaPosebnuUstanovu(txtSearch.Text, DajParametar(), zaposleni,ustanova);
                view.Refresh();
            }
        }
    }
}
