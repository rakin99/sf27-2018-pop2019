﻿using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for Prijava.xaml
    /// </summary>
    public partial class Prijava : Window
    {
        public Prijava()
        {
            InitializeComponent();
        }

        private void btnPrijaviSe_Click(object sender, RoutedEventArgs e)
        {
            Data.UcitavanjeKorisnika();
            Data.UcitavanjeProfesora();
            Data.UcitavanjeAsistenata();
            Data.UcitavanjeUstanova();
            Data.UcitavanjeTermina();
            String korisnicko = txtKorisnicko.Text;
            String lozinka = txtLozinka.Password.ToString();
            Data.ulogovaniKorisnik=Data.Prijava(korisnicko, lozinka);
            Korisnik korisnik = Data.ulogovaniKorisnik;
            
            if (korisnik == null)
            {
                MessageBox.Show($"Pogresno korisnicko ime ili lozinka!\nPokusajte ponovo.", "Greska", MessageBoxButton.OK);
                return;
            }

            switch (korisnik.TipKorisnika.ToString())
            {
                case "ADMINISTRATOR":
                    {
                        MainWindow mainWindow = new MainWindow();
                        mainWindow.ShowDialog();
                    }
                    break;
                case "PROFESOR":
                    {
                        korisnik.Ustanova = Data.DajUstanovuZaKorisnika(korisnik.KorisnickoIme);
                        Data.UcitavanjeTerminaZaKorisnika(korisnik);
                        TerminiWindow terminiWindow = new TerminiWindow();
                        terminiWindow.ShowDialog();
                    }
                    break;
                case "ASISTENT":
                    {
                        korisnik.Ustanova = Data.DajUstanovuZaKorisnika(korisnik.KorisnickoIme);
                        Data.UcitavanjeTerminaZaKorisnika(korisnik);
                        TerminiWindow terminiWindow = new TerminiWindow();
                        terminiWindow.ShowDialog();
                    }
                    break;
                default:
                    break;
            }
            txtKorisnicko.Text = "";
            txtLozinka.Password = "";
        }

        private void btnGost_Click(object sender, RoutedEventArgs e)
        {
            Data.UcitavanjeKorisnika();
            Data.UcitavanjeUstanova();
            Data.UcitavanjeTermina();
            GostWindow gostWindow = new GostWindow();
            gostWindow.ShowDialog();
        }
    }
}
