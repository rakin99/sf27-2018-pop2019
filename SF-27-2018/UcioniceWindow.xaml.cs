﻿using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for UcioniceWindow.xaml
    /// </summary>
    public partial class UcioniceWindow : Window
    {
        ICollectionView view;
        string selectedFilter = "Active";
        private Ustanova prosledjena;
        public UcioniceWindow(Ustanova ustanova)
        {
            InitializeComponent();
            dgUcionice.IsReadOnly = true;
            InitializeView(ustanova);
            RefreshParametara();
            view.Filter = CustomFilter;
            prosledjena = ustanova;
        }

        private bool CustomFilter(object obj)
        {
            Ucionica ucionica = obj as Ucionica;
            return ucionica.Active;
        }

        private void InitializeView(Ustanova ustanova)
        {
            view = CollectionViewSource.GetDefaultView(ustanova.Ucionice);
            dgUcionice.ItemsSource = view;
        }

        private void btnDodajUcionicu_Click(object sender, RoutedEventArgs e)
        {
            Ucionica ucionica = new Ucionica();
            ucionica.Ustanova = prosledjena;
            UcionicaAddEditWindow ucionicaWindow = new UcionicaAddEditWindow(ucionica);
            if (ucionicaWindow.ShowDialog() == true)
            {
                selectedFilter = "Active";
                view.Refresh();
            }
        }

        private void btnIzmeniUcionicu_Click(object sender, RoutedEventArgs e)
        {
            Ucionica ucionica = dgUcionice.SelectedItem as Ucionica;
            if (ucionica == null)
            {
                MessageBox.Show("Ustanova nije selektovana", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Ucionica oldUcionica = ucionica.Clone();
                UcionicaAddEditWindow ucionicaWindow = new UcionicaAddEditWindow(ucionica);
                if (ucionicaWindow.ShowDialog() == false)
                {
                    int indeks = 0;
                    if (ucionica.SifraUcionice == oldUcionica.SifraUcionice)
                    {
                        indeks = prosledjena.Ucionice.IndexOf(ucionica);
                    }
                    prosledjena.Ucionice[indeks] = oldUcionica;
                }
                selectedFilter = "Active";
                view.Refresh();
            }
        }

        private void btnObrisiUcionicu_Click(object sender, RoutedEventArgs e)
        {
            Ucionica ucionica = dgUcionice.SelectedItem as Ucionica;
            if (ucionica != null)
            {
                ucionica.DeleteUcionicu();
                selectedFilter = "Active";
                view.Refresh();
            }
            else
            {
                MessageBox.Show("Selektujte ustanovu koju zelite obrisati", "Warning", MessageBoxButton.OK);
            }
        }

        private void btnPretraga_Click(object sender, RoutedEventArgs e)
        {
            Data.PretragaUcionica(txtSearch.Text,prosledjena,"");
            view.Refresh();
        }

        private void dgUcionice_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Ustanova") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("SifraUcionice"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        public void RefreshParametara()
        {
            cmbParametar.Items.Add("broju ucionice");
            cmbParametar.Items.Add("broju mesta");
            cmbParametar.Items.Add("tipu ucionice");
            cmbParametar.SelectedIndex = 0;
        }

        private string DajParametar()
        {
            String parametarPretrage = "";
            if (cmbParametar.SelectedItem != null)
            {
                switch (cmbParametar.SelectedItem.ToString())
                {
                    case "broju ucionice":
                        {
                            parametarPretrage = "brojUcionice";
                        }
                        break;
                    case "broju mesta":
                        {
                            parametarPretrage = "brojMesta";
                        }
                        break;
                    case "tipu ucionice":
                        {
                            parametarPretrage = "tipUcionice";
                        }
                        break;
                    default:
                        break;
                }
            }
            return parametarPretrage;
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            Data.PretragaUcionica(txtSearch.Text, prosledjena, DajParametar());
            view.Refresh();
        }
    }
}