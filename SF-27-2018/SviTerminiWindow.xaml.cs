﻿using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for SviTerminiWindow.xaml
    /// </summary>
    public partial class SviTerminiWindow : Window
    {
        ICollectionView view;
        string selectedFilter = "Active";
        public SviTerminiWindow()
        {
            InitializeComponent();
            dgTermini.IsReadOnly = true;
            RefreshParametara();
            RefreshUstanova();
            InitializeView();
            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {
            Termin termin = obj as Termin;
            return termin.Active;
        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Termini);
            
            DataGridTextColumn column1 = new DataGridTextColumn();
            column1.Header = "Ucionica";
            column1.Width = new DataGridLength(1, DataGridLengthUnitType.Star);
            column1.Binding = new Binding("UcionicA.BrojUcionice");
            dgTermini.Columns.Add(column1);

            DataGridTextColumn column2 = new DataGridTextColumn();
            column2.Header = "Zauzeo";
            column2.Width = new DataGridLength(1, DataGridLengthUnitType.Star);
            column2.Binding = new Binding("Zauzeo.KorisnickoIme");
            dgTermini.Columns.Add(column2);

            dgTermini.ItemsSource = view;
        }

        private void dgTermini_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Datum") || e.PropertyName.Equals("UcionicA") || e.PropertyName.Equals("Zauzeo") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("SifraTermina"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        public void RefreshParametara()
        {
            cmbParametar.Items.Add("ucionici");
            cmbParametar.Items.Add("korisniku koji je zauzeo");
            cmbParametar.Items.Add("pocetku termina");
            cmbParametar.Items.Add("kraju termina");
            cmbParametar.Items.Add("danu u nedelji");
            cmbParametar.Items.Add("tipu nastave");
            cmbParametar.SelectedIndex = 0;
        }

        private string DajParametar()
        {
            String parametarPretrage = "";
            if (cmbParametar.SelectedItem != null)
            {
                switch (cmbParametar.SelectedItem.ToString())
                {
                    case "ucionici":
                        {
                            parametarPretrage = "sifra_Ucionice";
                        }
                        break;
                    case "korisniku koji je zauzeo":
                        {
                            parametarPretrage = "sifra_Korisnika";
                        }
                        break;
                    case "pocetku termina":
                        {
                            parametarPretrage = "pocetakTermina";
                        }
                        break;
                    case "kraju termina":
                        {
                            parametarPretrage = "krajTermina";
                        }
                        break;
                    case "danu u nedelji":
                        {
                            parametarPretrage = "dan";
                        }
                        break;
                    case "tipu nastave":
                        {
                            parametarPretrage = "tipNastave";
                        }
                        break;
                    default:
                        break;
                }
            }
            return parametarPretrage;
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            String parm = DajParametar();
            if (parm.Equals("pocetakTermina") || parm.Equals("krajTermina"))
            {
                Data.PretragaTerminaPoDatumu(txtSearch.Text, parm, Data.Termini);
                view.Refresh();
            }
            else
            {
                Data.PretragaTermina(txtSearch.Text, parm, Data.Termini);
                view.Refresh();
            }
        }

        public void RefreshUstanova()
        {
            List<string> ustanove = new List<string>();
            foreach (Ustanova u in Data.Ustanove)
            {
                cmbUstanova.Items.Add(u.NazivUstanove);
            }
        }

        private void cmbUstanova_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbUstanova.SelectedItem != null)
            {
                Ustanova ustanova = Data.DajUstanovuPoNazivu(cmbUstanova.SelectedItem.ToString());
                if (ustanova != null)
                {
                    Data.PretragaTermina(ustanova.SifraUstanove.ToString(), "ustanova", Data.Termini);
                    view.Refresh();
                }
            }
        }
    }
}
