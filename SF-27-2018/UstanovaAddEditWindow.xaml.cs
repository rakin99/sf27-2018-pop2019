﻿using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for UstanovaAddEditWindow.xaml
    /// </summary>
    public partial class UstanovaAddEditWindow : Window
    {
        enum Status { ADD, EDIT }
        private Status _status;
        private Ustanova selectedUstanova;
        public UstanovaAddEditWindow(Ustanova ustanova)
        {
            InitializeComponent();

            if (ustanova.NazivUstanove.Equals(""))
            {
                this._status = Status.ADD;
            }
            else
            {
                this._status = Status.EDIT;
            }
            selectedUstanova = ustanova;
            this.DataContext = ustanova;
        }

        private void btnOkay_Click(object sender, RoutedEventArgs e)
        {
            Ustanova ustanova = Data.DajUstanovuPoNazivu(txtNaziv.Text);
            if (ustanova != null && _status.Equals(Status.ADD) && ustanova.Active==true)
            {
                MessageBox.Show($"Ustanova sa ovim nazivom {ustanova.NazivUstanove} vec postoji", "Greska", MessageBoxButton.OK);
                return;
            }
            if (_status.Equals(Status.ADD))
            {
                if (ProveraPolja() == false)
                {
                    return;
                }
                Data.Ustanove.Add(selectedUstanova);
                selectedUstanova.SacuvajUstanovu();
            }
            if (_status.Equals(Status.EDIT))
            {
                ustanova = Data.DajUstanovuPoNazivuEdit(txtNaziv.Text,selectedUstanova);
                if (ustanova!=null && _status.Equals(Status.EDIT) && ustanova.Active == true)
                {
                    MessageBox.Show($"Ustanova sa ovim nazivom {ustanova.NazivUstanove} vec postoji", "Greska", MessageBoxButton.OK);
                    return;
                }
                if (ProveraPolja() == false)
                {
                    return;
                }
                selectedUstanova.UpdateUstanove();
            }
            this.DialogResult = true;
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        public bool ProveraPolja()
        {
            if (txtNaziv.Text.Equals("") || txtAdresa.Equals(""))
            {
                MessageBox.Show($"Sva polja moraju biti popunjena!", "Greska", MessageBoxButton.OK);
                return false;
            }
            return true;
        }
    }
}
