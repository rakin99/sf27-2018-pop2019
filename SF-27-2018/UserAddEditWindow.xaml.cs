﻿using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for UserAddEditWindow.xaml
    /// </summary>
    public partial class UserAddEditWindow : Window
    {

        enum Status { ADD,EDIT}
        private Status _status;
        private Korisnik selectedUser;
        public UserAddEditWindow(Korisnik user, String parm)
        {
            InitializeComponent();

            cmbType.ItemsSource = new List<ETipKorisnika>() { ETipKorisnika.ADMINISTRATOR,ETipKorisnika.PROFESOR,ETipKorisnika.ASISTENT};
            RefreshUstanova();
            RefreshProf();
            if (user.KorisnickoIme.Equals(""))
            {
                this._status = Status.ADD;
            }
            else if (parm.Equals("prikaz"))
            {
                txtName.IsEnabled = false;
                txtLastName.IsEnabled = false;
                txtMail.IsEnabled = false;
                txtUserName.IsEnabled = false;
                txtLozinka.Password = user.Lozinka;
                txtLozinka.IsEnabled = false;
                btnCancel.Visibility = Visibility.Hidden;
                btnOkay.Visibility = Visibility.Hidden;
                cmbType.IsEnabled = false;
                cmbUstanova.Items.Clear();
                cmbUstanova.Items.Add(user.Ustanova.NazivUstanove);
                cmbUstanova.SelectedIndex = 0;
                cmbUstanova.IsEnabled = false;
                if (user.TipKorisnika.ToString().Equals("ASISTENT"))
                {
                    Asistent asistent = (Asistent)user;
                    cmbProf.Items.Clear();
                    lblProf.Visibility = Visibility.Visible;
                    cmbProf.Visibility = Visibility.Visible;
                    if (asistent.Profesor == null)
                    {
                        cmbProf.Items.Add("NEDODELJEN");
                        cmbProf.SelectedIndex = 0;
                    }
                    else
                    {
                        cmbProf.Items.Add(asistent.Profesor.Ime + " " + asistent.Profesor.Prezime);
                        cmbProf.SelectedIndex = 0;
                    }
                    cmbProf.IsEnabled = false;
                }
            }
            else
            {
                txtLozinka.Password = user.Lozinka;
                lblProf.Visibility = Visibility.Hidden;
                cmbProf.Visibility = Visibility.Hidden;
                lblTypeofUser.Visibility = Visibility.Hidden;
                cmbType.Visibility = Visibility.Hidden;
                cmbUstanova.Visibility = Visibility.Hidden;
                lblUstanova.Visibility = Visibility.Hidden;
                this._status = Status.EDIT;
                txtUserName.IsReadOnly = true;
                
            }
            selectedUser = user;
            this.DataContext = user;
        }

        private void btnOkay_Click(object sender, RoutedEventArgs e)
        {
            Korisnik user = Data.DajKorisnikaPoUserNameu(txtUserName.Text);
            if (user !=null && _status.Equals(Status.ADD) && user.Active==true)
            {
                MessageBox.Show($"Korisnik sa korisnickim imenom {user.KorisnickoIme} vec postoji", "Warning", MessageBoxButton.OK);
                return;
            }
            if (_status.Equals(Status.ADD))
            {
                if (ProveraPolja() == false)
                {
                    return;
                }
                selectedUser.Lozinka = txtLozinka.Password.ToString();
                Data.Korisnici.Add(selectedUser);
                if (cmbType.Text.Equals("ASISTENT"))
                {
                    Profesor profesor = (Profesor)Data.DajKorisnikaPoUserNameu(cmbProf.Text);
                    Asistent asistent = new Asistent(selectedUser.Ime, selectedUser.Prezime, selectedUser.Email, selectedUser.KorisnickoIme, selectedUser.Lozinka, profesor, true);
                    Ustanova ustanova = Data.DajUstanovuPoNazivu(cmbUstanova.SelectedItem.ToString());
                    asistent.Ustanova = ustanova;
                    if (profesor != null)
                    {
                        asistent.SacuvajKorisnika(profesor.KorisnickoIme);
                    }
                    else
                    {
                        asistent.SacuvajKorisnika("");
                    }
                    
                }
                if (cmbType.Text.Equals("PROFESOR"))
                {
                    Profesor profesor = new Profesor(selectedUser.Ime, selectedUser.Prezime, selectedUser.Email, selectedUser.KorisnickoIme, selectedUser.Lozinka, true);
                    Ustanova ustanova = Data.DajUstanovuPoNazivu(cmbUstanova.SelectedItem.ToString());
                    profesor.Ustanova = ustanova;
                    profesor.SacuvajKorisnika(profesor.KorisnickoIme);

                }
                if (cmbType.Text.Equals("ADMINISTRATOR"))
                {
                    selectedUser.SacuvajKorisnika(selectedUser.KorisnickoIme);
                }
            }
            if (_status.Equals(Status.EDIT))
            {
                if (ProveraPoljaZaEdit() == false)
                {
                    return;
                }
                selectedUser.Lozinka = txtLozinka.Password.ToString();
                selectedUser.UpdateKorisnika();
            }
            this.DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void cmbType_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbType.Text.Equals("ADMINISTRATOR"))
            {
                lblUstanova.Visibility = Visibility.Hidden;
                cmbUstanova.Visibility = Visibility.Hidden;
                lblProf.Visibility = Visibility.Hidden;
                cmbProf.Visibility = Visibility.Hidden;
            }
            else if (cmbType.Text.Equals("ASISTENT"))
            {
                lblProf.Visibility = Visibility.Visible;
                cmbProf.Visibility = Visibility.Visible;
                lblUstanova.Visibility = Visibility.Visible;
                cmbUstanova.Visibility = Visibility.Visible;

            }
            else if (cmbType.Text.Equals("PROFESOR"))
            {
                lblProf.Visibility = Visibility.Hidden;
                cmbProf.Visibility = Visibility.Hidden;
                lblUstanova.Visibility = Visibility.Visible;
                cmbUstanova.Visibility = Visibility.Visible;
            }
        }

        public void RefreshUstanova()
        {
            List<string> ustanove = new List<string>();
            foreach (Ustanova u in Data.Ustanove)
            {
                cmbUstanova.Items.Add(u.NazivUstanove);
            }
        }

        public bool proveraEmaila()
        {
            int uslov1 = 0;
            int uslov2 = 0;
            string email = txtMail.Text;
            for (int i = 0; i < email.Count(); i++)
            {
                if ('@'.Equals(email[i]))
                {
                    uslov1 = 1;
                } 
                if ('.'.Equals(email[i]))
                {
                    string com = email.Split('.').GetValue(1).ToString();
                    if (com.Equals("com"))
                    {
                        uslov2 = 1;
                    }
                }
            }
            if (uslov1 + uslov2 == 2)
            {
                return true;
            }
            return false;
        }

        public bool ProveraPolja()
        {
            if (txtLozinka.Password.ToString().Equals("") || txtLastName.Text.Equals("") || txtName.Text.Equals("") || txtMail.Text.Equals("") || txtUserName.Text.Equals("") || cmbUstanova.SelectedItem == null)
            {
                MessageBox.Show($"Sva polja moraju biti popunjena!", "Greska", MessageBoxButton.OK);
                return false;
            }
            if (proveraEmaila() == false)
            {
                MessageBox.Show($"Neispravan e-mail!", "Greska", MessageBoxButton.OK);
                return false;
            }
            return true;
        }

        public bool ProveraPoljaZaEdit()
        {
            if (txtLozinka.Password.ToString().Equals("") || txtLastName.Text.Equals("") || txtName.Text.Equals("") || txtMail.Text.Equals(""))
            {
                MessageBox.Show($"Sva polja moraju biti popunjena!", "Greska", MessageBoxButton.OK);
                return false;
            }
            if (proveraEmaila() == false)
            {
                MessageBox.Show($"Neispravan e-mail!", "Greska", MessageBoxButton.OK);
                return false;
            }
            return true;
        }

        public void RefreshProf()
        {
            cmbProf.Items.Clear();
            foreach (Korisnik korisnik in Data.Korisnici)
            {
                if (cmbUstanova.SelectedItem != null)
                {
                    Ustanova ustanova = Data.DajUstanovuPoNazivu(cmbUstanova.SelectedItem.ToString());
                    if (korisnik.TipKorisnika == ETipKorisnika.PROFESOR && korisnik.Active == true && korisnik.Ustanova.SifraUstanove == ustanova.SifraUstanove)
                    {
                        cmbProf.Items.Add(korisnik.KorisnickoIme);
                    }
                }
            }
        }

        private void cmbUstanova_DropDownClosed(object sender, EventArgs e)
        {
            RefreshProf();
        }
    }
}
