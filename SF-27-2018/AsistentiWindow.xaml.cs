﻿using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_27_2018
{
    /// <summary>
    /// Interaction logic for AsistentiWindow.xaml
    /// </summary>
    public partial class AsistentiWindow : Window
    {

        Profesor profesor = new Profesor();
        ICollectionView view;
        string selectedFilter = "Active";
        public AsistentiWindow()
        {
            InitializeComponent();
            dgAsistenti.IsReadOnly = true;

            InitializeView();
            Refresh();
            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {
            Asistent asistent = obj as Asistent;
            return asistent.Active;
        }

        private void InitializeView()
        {
            foreach (Profesor p in Data.Profesori)
            {
                if (p.KorisnickoIme.Equals(Data.ulogovaniKorisnik.KorisnickoIme))
                {
                    profesor = p;
                }
            }
            view = CollectionViewSource.GetDefaultView(profesor.Asistenti);
            dgAsistenti.ItemsSource = view;
        }

        private void dgAsistenti_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Ustanova") || e.PropertyName.Equals("Termini") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("Id") || e.PropertyName.Equals("Profesor"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void btnUkloni_Click(object sender, RoutedEventArgs e)
        {
            Asistent asistent = dgAsistenti.SelectedItem as Asistent;
            if (asistent == null)
            {
                MessageBox.Show("Asistent nije selektovan!", "Greska", MessageBoxButton.OK);
                return;
            }
            else
            {
                profesor.Asistenti.Remove(asistent);
                asistent.Profesor = null;
                asistent.UpdateKorisnika();
                Data.Asistenti.Add(asistent);
            }
            Refresh();
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            int index = cmbAsistenti.Items.IndexOf(cmbAsistenti.SelectedItem);
            if (index == -1)
            {
                MessageBox.Show("Izaberite asistenta!", "Greska", MessageBoxButton.OK);
                return;
            }
            else
            {
                Asistent asistent = Data.DajAsistentaPoUserNameu(cmbAsistenti.SelectedItem.ToString());
                asistent.Profesor = profesor;
                asistent.UpdateKorisnika();
                profesor.Asistenti.Add(asistent);
                Data.Asistenti.Remove(asistent);
                Refresh();
            }
        }

        private void Refresh()
        {
            cmbAsistenti.Items.Clear();
            foreach (Asistent a in Data.Asistenti)
            {
                if (a.Active = true && a.Profesor==null && profesor.Ustanova.SifraUstanove==a.Ustanova.SifraUstanove)
                {
                    cmbAsistenti.Items.Add(a.KorisnickoIme);
                }
            }
        }
    }
}
