﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SF_27_2018
{
    public class EmailValidationRule:ValidationRule
    {
        Regex regex = new Regex(@"\b[A-Z0-9]{1,10}@[A-Z]{1,10}\.[A-Z]{2,3}\b", RegexOptions.IgnoreCase);
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string s = value as string;
            if (s == null || s.Equals(string.Empty))
                return new ValidationResult(false, "This field is required!");
            else if (regex.Match(s).Success)
                return new ValidationResult(true, null);
            return new ValidationResult(false, "Format is wrong!");
        }
    }
}
