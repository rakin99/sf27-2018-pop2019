﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;

namespace SF_27_2018_POP2019.Model
{
    [Serializable]
    public class Ucionica : INotifyPropertyChanged, IDataErrorInfo
    {
        
        private int sifraUcionice;
        private string brojUcionice;
        private int brojMesta;
        private ETipUcionice tipUcionice;
        private Ustanova ustanova;

        public Ucionica(int sifra, string brUcionice, int brMesta, ETipUcionice tip, Ustanova ustanova,bool active)
        {
            this.SifraUcionice = sifra;
            this.BrojUcionice = brUcionice;
            this.BrojMesta = brMesta;
            this.TipUcionice = tip;
            this.Ustanova = ustanova;
            this.Active = active;
        }

        public Ucionica()
        {
            this.SifraUcionice = 0;
            this.BrojUcionice = "";
            this.BrojMesta = 0;
            this.TipUcionice = ETipUcionice.SaRacunarima;
            this.Active = true;
            this.Ustanova = new Ustanova();
        }


        public int SifraUcionice
        {
            get { return sifraUcionice; }
            set { sifraUcionice = value; }
        }


        public string BrojUcionice
        {
            get { return brojUcionice; }
            set { brojUcionice = value; }
        }


        public int BrojMesta
        {
            get { return brojMesta; }
            set { brojMesta = value; }
        }


        public  ETipUcionice TipUcionice
        {
            get { return tipUcionice; }
            set { tipUcionice = value; }
        }

        private bool active;
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {

            get
            {
                switch (columnName)
                {
                    case "BrojUcionice":
                        if (BrojUcionice.Equals(string.Empty))
                            return "This field is required!";
                        break;
                    case "BrojMesta":
                        if (BrojMesta<=0)
                            return "This field is required!";
                        break;
                }
                return string.Empty;
            }
        }

        public Ustanova Ustanova
        {
            get { return ustanova; }
            set { ustanova = value; }
        }

        public Ucionica Clone()
        {
            Ucionica ucionica = new Ucionica(SifraUcionice, BrojUcionice, BrojMesta, TipUcionice, Ustanova,Active);
            return ucionica;
        }

        public int SacuvajUcionicu()
        {
            int id = -1;
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into ucionice (brojUcionice, brojMesta, tipUcionice, Active, sifra_Ustanove) output inserted.sifraUcionice" 
                                                    +" values (@brojUcionice, @brojMesta, @tipUcionice,@Active, @sifraUstanove)";

                command.Parameters.Add(new SqlParameter("sifraUcionice", this.SifraUcionice));
                command.Parameters.Add(new SqlParameter("brojUcionice", this.BrojUcionice));
                command.Parameters.Add(new SqlParameter("brojMesta", this.BrojMesta));
                command.Parameters.Add(new SqlParameter("tipUcionice", this.TipUcionice.ToString()));
                command.Parameters.Add(new SqlParameter("Active", this.Active));
                command.Parameters.Add(new SqlParameter("sifraUstanove", this.Ustanova.SifraUstanove));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }

        public void UpdateUcionicu()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update ucionice set brojUcionice=@brojUcionice, brojMesta=@brojMesta, tipUcionice=@tipUcionice where sifraUcionice=@sifraUcionice";

                command.Parameters.Add(new SqlParameter("sifraUcionice", this.SifraUcionice));
                command.Parameters.Add(new SqlParameter("brojUcionice", this.BrojUcionice));
                command.Parameters.Add(new SqlParameter("brojMesta", this.BrojMesta));
                command.Parameters.Add(new SqlParameter("tipUcionice", this.TipUcionice.ToString()));
                command.Parameters.Add(new SqlParameter("Active", this.Active));
              
                command.ExecuteNonQuery();
            }
        }

        public virtual void DeleteUcionicu()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update ucionice set active=@active where sifraUcionice=@sifraUcionice update termini set active=@active where sifra_Ucionice=@sifraUcionice";

                command.Parameters.Add(new SqlParameter("active", this.Active = false));
                command.Parameters.Add(new SqlParameter("sifraUcionice", this.SifraUcionice));

                command.ExecuteNonQuery();
            }
        }

    }
}
