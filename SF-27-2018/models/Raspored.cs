﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF_27_2018.models
{
    public class Raspored
    {
        private Ponedeljak ponedeljak;
        private Utorak utorak;
        private Sreda sreda;
        private Cetvrtak cetvrtak;
        private Petak petak;
        private Subota subota;
        private Nedelja nedelja;

        public Raspored()
        {
            this.Ponedeljak = new Ponedeljak();
            this.Utorak = new Utorak();
            this.Sreda = new Sreda();
            this.Cetvrtak = new Cetvrtak();
            this.Petak = new Petak();
            this.Subota = new Subota();
            this.Nedelja = new Nedelja();
        }

        public Ponedeljak Ponedeljak
        {
            get { return ponedeljak; }
            set { ponedeljak = value; }
        }

        public Utorak Utorak
        {
            get { return utorak; }
            set { utorak = value; }
        }

        public Sreda Sreda
        {
            get { return sreda; }
            set { sreda = value; }
        }

        public Cetvrtak Cetvrtak
        {
            get { return cetvrtak; }
            set { cetvrtak = value; }
        }

        public Petak Petak
        {
            get { return petak; }
            set { petak = value; }
        }

        public Subota Subota
        {
            get { return subota; }
            set { subota = value; }
        }

        public Nedelja Nedelja
        {
            get { return nedelja; }
            set { nedelja = value; }
        }
    }
}
