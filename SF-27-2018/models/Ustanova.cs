﻿using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF_27_2018_POP2019.Model
{
    [Serializable]
    public class Ustanova: INotifyPropertyChanged, IDataErrorInfo
    {
        private ObservableCollection<Ucionica> ucionice;

        public ObservableCollection<Ucionica> Ucionice
        {
            get { return ucionice; }
            set { ucionice = value; }
        }

        private int sifraUstanove;

        public int SifraUstanove
        {
            get { return sifraUstanove; }
            set { sifraUstanove = value; }
        }

        private string nazivUstanove;

        public string NazivUstanove
        {
            get { return nazivUstanove; }
            set { nazivUstanove = value; }
        }

        private string adresa;

        public string Adresa
        {
            get { return adresa; }
            set { adresa = value; }
        }

        private bool active;


        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {

            get
            {
                switch (columnName)
                {
                    case "NazivUstanove":
                        if (NazivUstanove.Equals(string.Empty))
                            return "This field is required!";
                        break;
                    case "Adresa":
                        if (Adresa.Equals(string.Empty))
                            return "This field is required!";
                        break;
                }
                return string.Empty;
            }
        }

        public Ustanova(int sifraUstanove, string nazivUstanova, string adresa, bool active)
        {
            this.SifraUstanove = sifraUstanove;
            this.NazivUstanove = nazivUstanova;
            this.Adresa = adresa;
            this.Active = active;
            this.Ucionice = new ObservableCollection<Ucionica>();
        }

        public Ustanova()
        {
            this.SifraUstanove = 0;
            this.NazivUstanove = "";
            this.Adresa = "";
            this.Active = true;
            this.Ucionice = new ObservableCollection<Ucionica>();
        }

        public Ustanova Clone()
        {
            Ustanova ustanova = new Ustanova(SifraUstanove, NazivUstanove, Adresa, Active);
            return ustanova;
        }

        public int SacuvajUstanovu()
        {
            int id = -1;
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into ustanove (Naziv, Adresa, Active) output inserted.sifraUstanove values (@Naziv, @Adresa, @Active)";

                command.Parameters.Add(new SqlParameter("Naziv", this.NazivUstanove));
                command.Parameters.Add(new SqlParameter("Adresa", this.Adresa));
                command.Parameters.Add(new SqlParameter("Active", this.Active));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }

        public void UpdateUstanove()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update ustanove set Naziv=@Naziv, Adresa=@Adresa where sifraUstanove=@sifraUstanove";

                command.Parameters.Add(new SqlParameter("Naziv", this.NazivUstanove));
                command.Parameters.Add(new SqlParameter("Adresa", this.Adresa));
                command.Parameters.Add(new SqlParameter("sifraUstanove", this.SifraUstanove));

                command.ExecuteNonQuery();
            }
        }

        public void DeleteUstanovu()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update ustanove set active=@active where sifraUstanove=@sifraUstanove";

                command.Parameters.Add(new SqlParameter("active", this.Active = false));
                command.Parameters.Add(new SqlParameter("sifraUstanove", this.SifraUstanove));

                command.ExecuteNonQuery();
            }
        }

    }
}
