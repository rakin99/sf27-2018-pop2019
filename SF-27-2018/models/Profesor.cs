﻿using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF_27_2018_POP2019.Model
{   [Serializable]
    public class Profesor:Korisnik
    {
        private ObservableCollection<Korisnik> asistenti;

        public ObservableCollection<Korisnik> Asistenti
        {
            get { return asistenti; }
            set { asistenti = value; }
        }

        public  Profesor (string ime, string prezime, string mail, string korisnicko, string lozinka, bool active) : base(ime, prezime,mail,korisnicko,lozinka, active)
        {
            TipKorisnika = ETipKorisnika.PROFESOR;
            this.asistenti = new ObservableCollection<Korisnik>();
        }

        public Profesor() : base()
        {
            TipKorisnika = ETipKorisnika.PROFESOR;
            this.asistenti = new ObservableCollection<Korisnik>();
        }

        public override Korisnik Clone()
        {
            Profesor profesor = new Profesor(Ime,Prezime,Email,KorisnickoIme,Lozinka, Active);
            profesor.Asistenti = Asistenti;
            return profesor;
        }

        public override int SacuvajKorisnika(String userName)
        {
            int id = base.SacuvajKorisnika(userName);

            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into profesori (id) values(@id)";
                command.Parameters.Add(new SqlParameter("id", id));

                command.ExecuteNonQuery();
            }
            return id;
        }

        public override int SelectedUserId()
        {
            return base.SelectedUserId();
        }

        public override void UpdateKorisnika()
        {
            base.UpdateKorisnika();
        }
        public override void DeleteKorisnika()
        {
            base.DeleteKorisnika();
        }
    }
}
