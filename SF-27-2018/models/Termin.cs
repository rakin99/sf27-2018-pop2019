﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF_27_2018_POP2019.Model;
using SF_27_2018_POP2019.Util;

namespace SF_27_2018_POP2019.Model
{
    [Serializable]
    public class Termin: INotifyPropertyChanged, IDataErrorInfo
    {
        private int sifraTermina;
        private DateTime pocetakTermina;
        private DateTime krajTermina;
        private EDani dan;
        private ETipNastave tipNastave;
        private Korisnik zauzeo;
        private Ucionica ucionica;
        private bool active;
        private String datum;

        public Termin(int sifra, DateTime pocetakTerimina,DateTime krajTermina, EDani danZauzeca,ETipNastave tip, Korisnik korisnik, Ucionica ucionica,bool active)
        {
            this.SifraTermina= sifra;
            this.PocetakTermina = pocetakTerimina;
            this.KrajTermina = krajTermina;
            this.Dan = danZauzeca;
            this.TipNastave = tip;
            this.Zauzeo = korisnik;
            this.UcionicA = ucionica;
            this.Active = active;
            if (Zauzeo != null)
            {
                this.Datum = this.ToString();
            }
        }

        public Termin()
        {
            this.SifraTermina = 0;
            this.PocetakTermina = new DateTime();
            this.KrajTermina = new DateTime();
            this.Dan = EDani.PONEDELJAK;
            this.TipNastave = ETipNastave.PREDAVANJA;
            this.Zauzeo = new Administrator();
            this.UcionicA = new Ucionica();
            this.Active = true;
            if (Zauzeo != null)
            {
                this.Datum = this.ToString();
            }
        }

        public int SifraTermina
        {
            get { return sifraTermina; }
            set { sifraTermina = value; }
        }

        public string Datum
        {
            get { return datum; }
            set { datum = value; }
        }

        public bool Active
        {
            get { return active; }
            set { active = value; }
        }


        public DateTime PocetakTermina
        {
            get { return pocetakTermina; }
            set { pocetakTermina = value; }
        }

        public DateTime KrajTermina
        {
            get { return krajTermina; }
            set { krajTermina = value; }
        }

        public EDani Dan
        {
            get { return dan; }
            set { dan = value; }
        }


        public ETipNastave TipNastave
        {
            get { return tipNastave; }
            set { tipNastave = value; }
        }


        public Korisnik Zauzeo
        {
            get { return zauzeo; }
            set { zauzeo = value; }
        }

        public Ucionica UcionicA
        {
            get { return ucionica; }
            set { ucionica = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {

            get
            {
                switch (columnName)
                {
                    case "PocetakTermina":
                        if (PocetakTermina.Equals(string.Empty))
                            return "This field is required!";
                        break;
                    case "KrajTermina":
                        if (KrajTermina.Equals(string.Empty))
                            return "This field is required!";
                        break;
                    case "Dan":
                        if (Dan.Equals(string.Empty))
                            return "This field is required!";
                        break;
                }
                return string.Empty;
            }
        }

        public virtual int SacuvajTermin()
        {
            int id = -1;
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Termini (pocetakTermina, krajTermina, dan, tipNastave, Active, sifra_Korisnika, sifra_Ucionice) output inserted.sifraTermina 
                                                    values (@pocetakTermina, @krajTermina, @dan, @tipNastave,@Active, @sifra_Korisnika, @sifra_Ucionice)";

                command.Parameters.Add(new SqlParameter("pocetakTermina", this.PocetakTermina));
                command.Parameters.Add(new SqlParameter("krajTermina", this.KrajTermina));
                command.Parameters.Add(new SqlParameter("dan", this.Dan.ToString()));
                command.Parameters.Add(new SqlParameter("tipNastave", this.TipNastave.ToString()));
                command.Parameters.Add(new SqlParameter("Active", this.Active));
                if (this.Zauzeo == null)
                {
                    command.Parameters.Add(new SqlParameter("sifra_Korisnika", ""));
                }
                else
                {
                    int sifra_Korisnika = Data.DajIdKorisnika(this.Zauzeo.KorisnickoIme);
                    command.Parameters.Add(new SqlParameter("sifra_Korisnika", sifra_Korisnika));
                }
                command.Parameters.Add(new SqlParameter("sifra_Ucionice", UcionicA.SifraUcionice));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }

        public void UpdateTermina()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Termini set sifra_Korisnika=@sifra_Korisnika, tipNastave=@tipNastave where sifraTermina=@sifraTermina and sifra_Ucionice=@sifra_Ucionice";

                int idKorisnika = Data.DajIdKorisnika(this.Zauzeo.KorisnickoIme);
                command.Parameters.Add(new SqlParameter("sifra_Korisnika", idKorisnika));
                command.Parameters.Add(new SqlParameter("tipNastave", this.TipNastave.ToString()));
                command.Parameters.Add(new SqlParameter("sifraTermina", this.SifraTermina));
                command.Parameters.Add(new SqlParameter("sifra_Ucionice", this.UcionicA.SifraUcionice));

                command.ExecuteNonQuery();
            }
        }

        public virtual void DeleteTermin()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update termini set Active=0 where sifraTermina=@sifraTermina";

                command.Parameters.Add(new SqlParameter("sifraTermina", this.SifraTermina));

                command.ExecuteNonQuery();
            }
        }

        public override string ToString()
        {
            return this.PocetakTermina.TimeOfDay+" - "+this.KrajTermina.TimeOfDay + " - "+this.UcionicA.BrojUcionice + " - " + this.Zauzeo.KorisnickoIme;
        }
    }
}
