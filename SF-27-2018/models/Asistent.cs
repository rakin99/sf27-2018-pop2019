﻿using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF_27_2018_POP2019.Model
{   [Serializable]
    public class Asistent : Korisnik
    {

        private Profesor profesor;

        public Profesor Profesor
        {
            get { return profesor; }
            set { profesor = value; }
        }

        public Asistent(string ime, string prezime, string mail, string korisnicko, string lozinka, Profesor profesor,bool active) : base(ime, prezime, mail, korisnicko, lozinka,active)
        {
            this.profesor = profesor;
            TipKorisnika = ETipKorisnika.ASISTENT;
        }

        public Asistent() : base ()
        {
            TipKorisnika = ETipKorisnika.ASISTENT;
            Profesor profesor = new Profesor();
        }

        public override Korisnik Clone()
        {
            Asistent asistent = new Asistent(Ime, Prezime, Email, KorisnickoIme, Lozinka,Profesor,Active);
            return asistent;
        }

        public override int SacuvajKorisnika(String userName)
        {
            int id = base.SacuvajKorisnika(userName);

            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into asistenti (id,Profesor_Id) values(@id,(select id from korisnici where korisnickoime=@userName))";
                command.Parameters.Add(new SqlParameter("id", id));
                command.Parameters.Add(new SqlParameter("userName", userName));

                command.ExecuteNonQuery();
            }
            return id;
        }

        public override int SelectedUserId()
        {
            return base.SelectedUserId();
        }

        public override void UpdateKorisnika()
        {
            base.UpdateKorisnika();
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update asistenti set Profesor_Id=(select Id from Korisnici where KorisnickoIme=@pKorisnicko and Active=1) 
                                            where Id=(select Id from Korisnici where KorisnickoIme=@KorisnickoIme and Active=1)";
                if (this.Profesor == null)
                {
                    command.Parameters.Add(new SqlParameter("pKorisnicko", ""));
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("pKorisnicko", this.Profesor.KorisnickoIme));
                }
                command.Parameters.Add(new SqlParameter("KorisnickoIme", this.KorisnickoIme));

                command.ExecuteNonQuery();
            }
        }
        public override void DeleteKorisnika()
        {
            base.DeleteKorisnika();
        }

    }
}
