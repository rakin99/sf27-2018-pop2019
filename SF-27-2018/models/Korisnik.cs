﻿using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF_27_2018_POP2019.Model
{   [Serializable]
    public abstract class Korisnik:INotifyPropertyChanged, IDataErrorInfo
    {
        private ObservableCollection<Termin> termini;

        public ObservableCollection<Termin> Termini
        {
            get { return termini; }
            set { termini = value; }
        }

        private Ustanova ustanova;
        public Ustanova Ustanova
        {
            get { return ustanova; }
            set { ustanova = value; }
        }

        private string id;
        public string Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged("Id"); }
        }

        private string name;

        public string Ime
        {
            get { return name; }
            set {name = value; OnPropertyChanged("Ime"); }
        }

        private string prezime;

        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; OnPropertyChanged("Prezime"); }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; OnPropertyChanged("Email"); }
        }

        private string username;

        public string KorisnickoIme
        {
            get { return username; }
            set { username = value; OnPropertyChanged("KorisnickoIme"); }
        }

        private string lozinka;

        public string Lozinka
        {
            get { return lozinka; }
            set { lozinka = value; OnPropertyChanged("Lozinka"); }
        }

        private ETipKorisnika tipKorisnika;

        public ETipKorisnika TipKorisnika

        {
            get { return tipKorisnika; }
            set { tipKorisnika = value; OnPropertyChanged("TipKorisnika"); }
        }

        private bool active;


        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public string Error
        {
            get { return null; }
        }

        //public string Error => throw new NotImplementedException();

        public string this[string columnName]
        {

            get
            {
                switch (columnName)
                {
                    case "Ime":
                        if (Ime.Equals(string.Empty))
                            return "This field is required!";
                        break;
                    case "Prezime":
                        if (Prezime.Equals(string.Empty))
                            return "This field is required!";
                        break;
                    case "Email":
                        if (Email.Equals(string.Empty))
                            return "This field is required!";
                        break;
                    case "KorisnickoIme":
                        if (KorisnickoIme.Equals(string.Empty))
                            return "This field is required!";
                        break;
                    case "Lozinka":
                        if (Lozinka.Equals(string.Empty))
                            return "This field is required!";
                        break;
                }
                return string.Empty;
            }
        }
        //public string this[string columnName] => throw new NotImplementedException();

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        public Korisnik(string ime, string prezime, string mail, string korisnicko, string lozinka, bool active)
        {
            this.Ime = ime;
            this.Prezime = prezime;
            this.Email = mail;
            this.KorisnickoIme = korisnicko;
            this.Lozinka = lozinka;
            this.Active = active;
            this.Termini = new ObservableCollection<Termin>();
        }

        public virtual Korisnik Clone()
        {
            return null;
        }

        public Korisnik()
        {
            Ime = "";
            Prezime = "";
            Email = "";
            KorisnickoIme = "";
            Lozinka = "";
            Active = true;
            this.Termini = new ObservableCollection<Termin>();
        }
        public virtual int SacuvajKorisnika(String userName)
        {
            int id = -1;
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into korisnici (Ime, Prezime, Email, KorisnickoIme, Lozinka, TipKorisnika, Active, sifra_Ustanove) output inserted.id values (@Ime, @Prezime, @Email, @KorisnickoIme,@Lozinka, @TipKorisnika, @Active,@sifra_Ustanove)";

                command.Parameters.Add(new SqlParameter("Ime", this.Ime));
                command.Parameters.Add(new SqlParameter("Prezime", this.Prezime));
                command.Parameters.Add(new SqlParameter("Email", this.Email));
                command.Parameters.Add(new SqlParameter("KorisnickoIme", this.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("Lozinka", this.Lozinka));
                command.Parameters.Add(new SqlParameter("TipKorisnika", this.TipKorisnika.ToString()));
                command.Parameters.Add(new SqlParameter("Active", this.Active));
                if (this.Ustanova == null)
                {
                    command.Parameters.Add(new SqlParameter("sifra_Ustanove", ""));
                }
                else
                {
                    command.Parameters.Add(new SqlParameter("sifra_Ustanove", this.Ustanova.SifraUstanove));
                }

                id = (int)command.ExecuteScalar();
            }
            return id;
        }

        public virtual void UpdateKorisnika()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update korisnici set Ime=@ime, Prezime=@prezime, Email=@email, Lozinka=@lozinka where KorisnickoIme=@korisnickoime";

                command.Parameters.Add(new SqlParameter("ime", this.Ime));
                command.Parameters.Add(new SqlParameter("prezime", this.Prezime));
                command.Parameters.Add(new SqlParameter("email", this.Email));
                command.Parameters.Add(new SqlParameter("lozinka", this.Lozinka));
                command.Parameters.Add(new SqlParameter("korisnickoime", this.KorisnickoIme));

                command.ExecuteNonQuery();
            }
        }

        public virtual void DeleteKorisnika()
        {
            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update korisnici set active=@active where KorisnickoIme=@korisnickoime";

                command.Parameters.Add(new SqlParameter("active", this.Active=false));
                command.Parameters.Add(new SqlParameter("korisnickoime", this.KorisnickoIme));

                command.ExecuteNonQuery();
            }
        }

        public virtual int SelectedUserId()
        {
            int id = -1;

            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"Select id from users where KorisnickoIme=@KorisnickiIme";
                command.Parameters.Add(new SqlParameter("KorisnickoIme", this.KorisnickoIme));

                id = (int)command.ExecuteScalar();
            }

            return id;
        }
    }

}
