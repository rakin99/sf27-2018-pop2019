﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF_27_2018.models
{
    public class Sedmica
    {
        private DateTime pocetakNedelje;
        private DateTime krajNedelje;

        public Sedmica(DateTime pocetak,DateTime kraj)
        {
            this.PocetakNedelje = pocetak;
            this.KrajNedelje = kraj;
        }

        public Sedmica()
        {
            this.PocetakNedelje = new DateTime();
            this.KrajNedelje = new DateTime();
        }

        public DateTime PocetakNedelje
        {
            get { return pocetakNedelje; }
            set { pocetakNedelje = value; }
        }

        public DateTime KrajNedelje
        {
            get { return krajNedelje; }
            set { krajNedelje = value; }
        }

        public override string ToString()
        {
            return this.PocetakNedelje.ToShortDateString() + " - "+this.KrajNedelje.ToShortDateString();
        }
    }
}
