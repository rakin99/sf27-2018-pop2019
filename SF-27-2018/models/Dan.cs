﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF_27_2018.models
{
    [Serializable]
    public class Dan
    {
        private string datum;
        private string zauzeo;
        private string ucionica;

        public Dan()
        {
            this.Datum = "";
            this.Zauzeo = "";
            this.Ucionica = "";
        }

        public string Datum
        {
            get { return datum; }
            set { datum = value; }
        }

        public string Zauzeo
        {
            get { return zauzeo; }
            set { zauzeo = value; }
        }

        public string Ucionica
        {
            get { return ucionica; }
            set { ucionica = value; }
        }
    }
}
