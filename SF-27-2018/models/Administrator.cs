﻿using SF_27_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF_27_2018_POP2019.Model
{   [Serializable]
    class Administrator : Korisnik
    {
        public Administrator(string ime, string prezime, string mail, string korisnicko, string lozinka,bool active) : base(ime, prezime, mail, korisnicko, lozinka, active)
        {
            TipKorisnika = ETipKorisnika.ADMINISTRATOR;
        }

        public override Korisnik Clone()
        {
            Administrator administrator = new Administrator(Ime, Prezime, Email, KorisnickoIme, Lozinka,Active);
            return administrator;
        }

        public Administrator() : base()
        {
            TipKorisnika = ETipKorisnika.ADMINISTRATOR;
        }

        public override int SacuvajKorisnika(String userName)
        {
            int id = base.SacuvajKorisnika(userName);

            using (SqlConnection conn = new SqlConnection(Data.Connection_string))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into administratori (id) values(@id)";
                command.Parameters.Add(new SqlParameter("id", id));
                Console.WriteLine("Administrator");

                command.ExecuteNonQuery();
            }
            return id;
        }

        public override int SelectedUserId()
        {
            return base.SelectedUserId();
        }

        public override void UpdateKorisnika()
        {
            base.UpdateKorisnika();
        }
        public override void DeleteKorisnika()
        {
            base.DeleteKorisnika();
        }

    }
}
