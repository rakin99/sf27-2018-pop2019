USE [master]
GO
/****** Object:  Table [dbo].[Administratori]    Script Date: 1/25/2020 4:57:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Administratori](
	[Id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Asistenti]    Script Date: 1/25/2020 4:57:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asistenti](
	[Id] [int] NOT NULL,
	[Profesor_Id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Korisnici]    Script Date: 1/25/2020 4:57:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Korisnici](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[KorisnickoIme] [varchar](50) NOT NULL,
	[Ime] [varchar](50) NOT NULL,
	[Prezime] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Lozinka] [varchar](30) NOT NULL,
	[TipKorisnika] [varchar](15) NOT NULL,
	[Active] [bit] NOT NULL,
	[sifra_Ustanove] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Profesori]    Script Date: 1/25/2020 4:57:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profesori](
	[Id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Termini]    Script Date: 1/25/2020 4:57:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Termini](
	[sifraTermina] [int] IDENTITY(1,1) NOT NULL,
	[pocetakTermina] [smalldatetime] NOT NULL,
	[krajTermina] [smalldatetime] NOT NULL,
	[dan] [varchar](10) NOT NULL,
	[tipNastave] [varchar](20) NULL,
	[Active] [bit] NOT NULL,
	[sifra_Korisnika] [int] NULL,
	[sifra_Ucionice] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sifraTermina] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ucionice]    Script Date: 1/25/2020 4:57:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ucionice](
	[sifraUcionice] [int] IDENTITY(1,1) NOT NULL,
	[brojUcionice] [varchar](20) NOT NULL,
	[brojMesta] [int] NOT NULL,
	[tipUcionice] [varchar](30) NOT NULL,
	[Active] [bit] NOT NULL,
	[sifra_Ustanove] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sifraUcionice] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ustanove]    Script Date: 1/25/2020 4:57:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ustanove](
	[sifraUstanove] [int] IDENTITY(1,1) NOT NULL,
	[Naziv] [varchar](50) NOT NULL,
	[Adresa] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sifraUstanove] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Administratori]  WITH CHECK ADD  CONSTRAINT [fk_korisnik_administrator] FOREIGN KEY([Id])
REFERENCES [dbo].[Korisnici] ([Id])
GO
ALTER TABLE [dbo].[Administratori] CHECK CONSTRAINT [fk_korisnik_administrator]
GO
ALTER TABLE [dbo].[Asistenti]  WITH CHECK ADD  CONSTRAINT [fk_korisnici_asistenti] FOREIGN KEY([Id])
REFERENCES [dbo].[Korisnici] ([Id])
GO
ALTER TABLE [dbo].[Asistenti] CHECK CONSTRAINT [fk_korisnici_asistenti]
GO
ALTER TABLE [dbo].[Profesori]  WITH CHECK ADD  CONSTRAINT [fk_korisnici_profesor] FOREIGN KEY([Id])
REFERENCES [dbo].[Korisnici] ([Id])
GO
ALTER TABLE [dbo].[Profesori] CHECK CONSTRAINT [fk_korisnici_profesor]
GO
ALTER TABLE [dbo].[Ucionice]  WITH CHECK ADD  CONSTRAINT [fk_ucionice_ustanove] FOREIGN KEY([sifra_Ustanove])
REFERENCES [dbo].[Ustanove] ([sifraUstanove])
GO
ALTER TABLE [dbo].[Ucionice] CHECK CONSTRAINT [fk_ucionice_ustanove]
GO
