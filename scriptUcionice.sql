﻿drop table Ucionice

create table dbo.Ucionice (
sifraUcionice int not null identity(1,1) primary key, 
brojUcionice varchar(20) not null, 
brojMesta int not null, 
tipUcionice varchar(30) not null, 
Active bit not null,
sifra_Ustanove int not null,
constraint fk_ucionice_ustanove foreign key (sifra_Ustanove) references dbo.Ustanove(sifraUstanove))

insert into Ucionice(brojUcionice,brojMesta,tipUcionice,Active,sifra_Ustanove)
values('INT-15',10,'Sa racunarima',1,1)

select *
from Ucionice