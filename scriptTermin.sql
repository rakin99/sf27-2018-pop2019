﻿drop table Termini

create table dbo.Termini (
sifraTermina int not null identity(1,1) primary key, 
pocetakTermina smalldatetime not null, 
krajTermina smalldatetime not null,
dan varchar(10) not null, 
tipNastave varchar(20),
Active bit not null,
sifra_Korisnika int null,
sifra_Ucionice int not null)

insert into Termini(pocetakTermina,krajTermina,dan,tipNastave,Active,sifra_Korisnika,sifra_Ucionice) values ('2020-01-12 12:35:00','2020-01-12 14:00:00','PONEDELJAK','PREDAVANJA', 1,1002,1)
insert into Termini(pocetakTermina,krajTermina,dan,tipNastave,Active,sifra_Korisnika,sifra_Ucionice) 
values ('2020-01-13 08:35:00','2020-01-13 11:00:00','UTORAK','VEZBE', 1,2002,2)

select *
from Termini
