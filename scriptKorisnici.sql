﻿drop table Korisnici
drop table Administratori
drop table Profesori

create table dbo.Korisnici (
Id int not null identity(1,1) primary key,
KorisnickoIme varchar(50) not null, 
Ime varchar(50) not null, 
Prezime varchar(50) not null, 
Email varchar(50) not null,
Lozinka varchar(30) not null,
TipKorisnika varchar(15) not null, 
Active bit not null,
sifra_Ustanove int null)

create table dbo.Administratori (
Id int not null primary key, constraint 
fk_korisnici_administratori foreign key (Id) references dbo.Korisnici(Id))

create table dbo.Profesori (
Id int not null primary key, 
constraint fk_korisnici_profesori foreign key(Id) references dbo.Korisnici(id))

create table dbo.Asistenti(
Id int not null primary key,
Id 
Profesor_Id int null, 
constraint fk_korisnici_tas foreign key (Id) references dbo.Korisnici(Id))

insert into dbo.Korisnici(Ime, Prezime, Email, KorisnickoIme, Lozinka, TipKorisnika, Active) 
values('Pera' ,'Peric', 'pera@gmail.com','pera123','peric321', 'ADMINISTRATOR' , 1);

insert into dbo.Administratori(Id) values(1);